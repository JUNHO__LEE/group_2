#!/bin/bash

git pull > temp.txt 2>>temp.txt

PULL_NUM=`cat temp.txt | wc -l`

if [ $PULL_NUM -eq 3] ; then
	echo "더이상 pull 할 것이 없습니다."
	rm -f temp.txt

else
	echo "업데이트 되었습니다."
	rm -f temp.txt
fi
