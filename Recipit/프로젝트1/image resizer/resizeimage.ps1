Import-Module .\Resize-Image.psm1
$INPUTJPGFILE = read-host "변환하려는 파일의 경로를 입력하세요"
$OUTPUTJPGFILE = read-host "변환되는 결과의 파일의 이름을 경로로 입력하세요"
$OUTPUTJPGWIDTH = read-host "변환되는 결과의 파일의 WIDTH 값을 입력하세요"
$OUTPUTJPGHEIGHT = read-host "변환되는 결과의 파일의 HEIGHT 값을 입력하세요"

Resize-Image -InputFile $INPUTJPGFILE -OutputFile $OUTPUTJPGFILE -Width $OUTPUTJPGWIDTH -Height $OUTPUTJPGHEIGHT