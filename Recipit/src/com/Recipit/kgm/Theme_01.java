

package com.Recipit.kgm;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class Theme_01 extends JPanel{
	JPanel p1 = new JPanel();
	JPanel p2 = new JPanel();
	JPanel p3 = new JPanel();
	JPanel p4 = new JPanel();
	
	JLabel l1 = new JLabel("테마레시피");
	JLabel l2 = new JLabel("업데이트레시피");
	JLabel l3 = new JLabel("인기레시피");
	
	Font f1 = new Font("돋움",Font.BOLD,16);

	JButton mb1 = new JButton(new ImageIcon("kgmbtn/dosirak.jpg"));
	JButton mb2 = new JButton(new ImageIcon("kgmbtn/iusik.jpg"));
	JButton mb3 = new JButton(new ImageIcon("kgmbtn/myeong.jpg"));
	JButton mb4 = new JButton(new ImageIcon("kgmbtn/gan.jpg"));
	
	JButton imgb1 = new JButton(new ImageIcon("이미지"));
	JButton imgb2 = new JButton(new ImageIcon("이미지"));
	JButton imgb3 = new JButton(new ImageIcon("이미지"));

	JButton imgt1 = new JButton(new ImageIcon("이미지"));
	JButton imgt2 = new JButton(new ImageIcon("이미지"));
	JButton imgt3 = new JButton(new ImageIcon("이미지"));
	
	JButton more1 = new JButton(new ImageIcon("kgmbtn/more.jpg"));
	JButton more2 = new JButton(new ImageIcon("kgmbtn/more.jpg"));
	JButton more3 = new JButton(new ImageIcon("kgmbtn/more.jpg"));
	CardLayout cardLayout = new CardLayout();
	
	public Theme_01() {
		setLayout(cardLayout);
		p4.setLayout(new GridLayout(3, 0, 0, 10));
		
		// 패널 가져오기
		JPanel p5 = new RecipeList();
		// JPanel p6 = new ?????(this);
		
		//폰트&사이즈 설정1
		l1.setFont(f1);
		l1.setPreferredSize(new Dimension(190,30));
		more1.setPreferredSize(new Dimension(60, 30));
		mb1.setPreferredSize(new Dimension(125, 35));
		mb2.setPreferredSize(new Dimension(125, 35));
		mb3.setPreferredSize(new Dimension(125, 35));
		mb4.setPreferredSize(new Dimension(125, 35));
		more1.setBackground(Color.WHITE);
		//more1.setBorderPainted(false);
		
		//패널 설정
		p1.setBackground(Color.WHITE);
		p1.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
		p1.setPreferredSize(new Dimension(300, 150));
		p1.setBorder(new LineBorder(Color.BLACK));
		//패널에 붙임
		p1.add(l1);
		p1.add(more1);
		p1.add(mb1);
		p1.add(mb2);
		p1.add(mb3);
		p1.add(mb4);
		
		//폰트&사이즈 설정2
		l2.setFont(f1);
		l2.setPreferredSize(new Dimension(190,30));
		more2.setPreferredSize(new Dimension(60, 30));
		imgb1.setPreferredSize(new Dimension(80, 70));
		imgb2.setPreferredSize(new Dimension(80, 70));
		imgb3.setPreferredSize(new Dimension(80, 70));
		//패널 설정
		p2.setBackground(Color.WHITE);
		p2.setLayout(new FlowLayout(FlowLayout.CENTER,10,10));
		p2.setPreferredSize(new Dimension(300, 150));
		p2.setBorder(new LineBorder(Color.BLACK));
		//패널에 붙임
		p2.add(l2);
		p2.add(more2);
		p2.add(imgb1);
		p2.add(imgb2);
		p2.add(imgb3);
	/*	Box box = Box.createVerticalBox();
		add(box.createVerticalStrut(20));
		
		Box box2 = Box.createHorizontalBox();
		add(box2.createHorizontalStrut(5));*/
		
		
		//폰트&사이즈설정3
		l3.setFont(f1);
		l3.setPreferredSize(new Dimension(190, 30));
		more3.setPreferredSize(new Dimension(60, 30));
		imgt1.setPreferredSize(new Dimension(80, 70));
		imgt2.setPreferredSize(new Dimension(80, 70));
		imgt3.setPreferredSize(new Dimension(80, 70));
		//패널 설정
		p3.setBackground(Color.WHITE);
		p3.setLayout(new FlowLayout(FlowLayout.CENTER,10,10));
		p3.setBorder(new LineBorder(Color.BLACK));
		p3.setPreferredSize(new Dimension(300, 150));
		//패널에 붙임
		p3.add(l3);
		p3.add(more3);
		p3.add(imgt1);
		p3.add(imgt2);
		p3.add(imgt3);

		p4.add(p1);
		/*Box box2 = Box.createVerticalBox();
		p4.add(box2.createVerticalStrut(5));
		p4.add(box2.createHorizontalStrut(5));*/
		
		p4.add(p2);
		/*Box box3 = Box.createVerticalBox();
		p4.add(box3.createVerticalStrut(5));
		p4.add(box3.createHorizontalStrut(5));
		*/
		p4.add(p3);
		
		this.add(p4,"01");
		this.add(p5,"업데이트레시피");
		//this.add(p6, "인기레시피");
		
		// 버튼 이벤트 설정
		more2.addActionListener((e)->{
			cardLayout.show(this, "업데이트레시피");
		});
		
		/*more3.addActionListener((e)->{
			cardLayout.show(this, "인기레시피");
		});*/
	}
	
}

