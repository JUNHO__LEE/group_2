package com.Recipit.kgm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

class Theme_02 extends JPanel{
	public Theme_02() {
		setLayout(new BorderLayout(0,0));
		JPanel jp1 = new JPanel();
		add(jp1, BorderLayout.CENTER);
		setBackground(Color.white);
		jp1.setLayout(new FlowLayout(FlowLayout.LEFT,10,5));
		
		
	JPanel jp2 = new JPanel();
	JPanel jp3 = new JPanel();
	
	JLabel jl = new JLabel("테마레시피");
	Font jf = new Font("돋움", Font.BOLD, 16);
	JButton jb1 = new JButton(new ImageIcon("kgmbtn/dosirak.jpg"));
	JButton jb2 = new JButton(new ImageIcon("kgmbtn/iusik.jpg"));
	JButton jb3 = new JButton(new ImageIcon("kgmbtn/myeong.jpg"));
	JButton jb4 = new JButton(new ImageIcon("kgmbtn/gan.jpg"));
	JButton plus = new JButton(new ImageIcon("kgmbtn/plus.jpg"));
	///////////
	jl.setFont(jf);
	jl.setPreferredSize(new Dimension(215, 30));
	plus.setPreferredSize(new Dimension(35, 30));
	jb1.setPreferredSize(new Dimension(125, 30));
	jb2.setPreferredSize(new Dimension(125, 30));
	jb3.setPreferredSize(new Dimension(125, 30));
	jb4.setPreferredSize(new Dimension(125, 30));
	
	jp1.setBackground(Color.WHITE);
	jp1.add(jp2);
	jp1.add(jp3);
	
	jp1.setPreferredSize(new Dimension(300,500));
	jp1.setBorder(new LineBorder(Color.BLACK));
	
	///////////
	jp2.add(jl);
	
	}
}


public class ThemeMore extends JFrame{
	public static void main(String[] args) {
		JFrame jf = new JFrame();
		jf.setLayout(new BoxLayout(jf.getContentPane(), BoxLayout.Y_AXIS));
		jf.add(new Theme_02());
		jf.setBounds(600,300,300,600);
		jf.setDefaultCloseOperation(EXIT_ON_CLOSE);
		jf.setVisible(true);
		jf.setResizable(false);
	}
}
