
package com.Recipit.kgm;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

class Theme01 extends JFrame{
	JPanel p1 = new JPanel();
	JPanel p2 = new JPanel();
	JPanel p3 = new JPanel();
	
	JLabel l1 = new JLabel("테마레시피");
	JLabel l2 = new JLabel("업데이트레시피");
	JLabel l3 = new JLabel("인기레시피");
	
	
	Font f1 = new Font("바탕",Font.BOLD,16);
	Font f2 = new Font("바탕",Font.BOLD,16);
	Font f3 = new Font("바탕",Font.BOLD,16);
	
	JButton more1 = new JButton(new ImageIcon("kgmbtn/more.jpg"));
	JButton mb1 = new JButton(new ImageIcon("kgmbtn/dosirak.jpg"));
	JButton b3 = new JButton(new ImageIcon("kgmbtn/iusik.jpg"));
	JButton b4 = new JButton(new ImageIcon("kgmbtn/myeong.jpg"));
	JButton b5 = new JButton(new ImageIcon("kgmbtn/gan.jpg"));

	
	JButton more2 = new JButton("kgmbtn/more.jpg");
	JButton b7 = new JButton("이미지");
	JButton b8 = new JButton("이미지");
	JButton b9 = new JButton("이미지");
	
	JButton more3 = new JButton("kgmbtn/more.jpg");
	JButton b11 = new JButton("이미지");
	JButton b12 = new JButton("이미지");
	JButton b13 = new JButton("이미지");
	
	public Theme01() {
		setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		
		l1.setFont(f1);
		l1.setPreferredSize(new Dimension(190,30));
		more1.setPreferredSize(new Dimension(60, 30));
		
		mb1.setPreferredSize(new Dimension(125, 35));
		b3.setPreferredSize(new Dimension(125, 35));
		b4.setPreferredSize(new Dimension(125, 35));
		b5.setPreferredSize(new Dimension(125, 35));
		b5.setHorizontalTextPosition(SwingConstants.LEFT);
		more1.setBackground(Color.WHITE);
		more1.setBorderPainted(false);

		
		//p1.setBounds(40, 40, 40, 40);
		p1.setBackground(Color.WHITE);
		p1.setLayout(new FlowLayout(FlowLayout.CENTER, 10,10));
		p1.add(l1);
		p1.add(more1);
		p1.add(mb1);
		p1.add(b3);
		p1.add(b4);
		p1.add(b5);
		this.add(p1);

		this.setBounds(600, 300, 300, 600);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setResizable(false);
	}
}