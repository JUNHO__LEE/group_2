package com.Recipit.kgm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

public class RecipeList extends JPanel{
	
	public RecipeList() {
		
		setLayout(new BorderLayout());
		
		//[<- 업데이트레시피 ] North 패널
		JPanel p1 = new JPanel();
		//Center 패널
		JPanel p2 = new JPanel();
		//p2 안 스크롤패널
		JScrollPane scrollPane = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		JPanel p3 = new JPanel();
		//스크롤 패널 안
		JPanel p4 = new JPanel();
		JPanel p5 = new JPanel();
		JPanel p6 = new JPanel();
		//p4,5,6 패널 안 (레시피이름, 추천수/조회수)
		JPanel p7 = new JPanel();
		JPanel p8 = new JPanel();
		
		JPanel p9 = new JPanel();
		JPanel p10 = new JPanel();
		
		JPanel p11 = new JPanel();
		JPanel p12 = new JPanel();
		//버튼
		JButton btn = new JButton(new ImageIcon("kgmbtn/arrow.jpg"));
		JButton btn1 = new JButton("이미지");
		JButton btn2 = new JButton("이미지");
		JButton btn3 = new JButton("이미지");
		//라벨
		JLabel lup2 = new JLabel("업데이트레시피");
		JLabel lr1 = new JLabel("레시피이름");
		JLabel lr2 = new JLabel("레시피이름1");
		JLabel lr3 = new JLabel("레시피이름2");
		JLabel lr4 = new JLabel("추천수 300");
		JLabel lr5 = new JLabel("조회수 300");
		JLabel lr6 = new JLabel("추천수 200");
		JLabel lr7 = new JLabel("조회수 300");
		JLabel lr8 = new JLabel("추천수 100");
		JLabel lr9 = new JLabel("조회수 50");
		////////////////////////////////////////////////설정
		
		//arrow버튼 크기 설정
		btn.setPreferredSize(new Dimension(40,30));
		//업데이트 글씨크기, 정렬, 크기설정
		lup2.setFont(new Font("돋움",Font.PLAIN, 14));
		lup2.setHorizontalAlignment(SwingConstants.CENTER);
		lup2.setPreferredSize(new Dimension(200,30));
		//패널
		p1.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 5));
		p1.setBorder(new LineBorder(Color.BLACK,2));
		p1.setBackground(Color.WHITE);

		p2.setLayout(new BorderLayout());
		
		p3.setBackground(Color.WHITE);
		scrollPane.setViewportView(p3);
		p3.setAutoscrolls(true);
		p3.setPreferredSize(new Dimension(280, 1000));
		p3.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 10));
		
		p4.setPreferredSize(new Dimension(260, 200));
		p4.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 5));
		
		p5.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 5));
		p5.setPreferredSize(new Dimension(260, 200));
		
		p6.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 5));
		p6.setPreferredSize(new Dimension(260, 200));
		
		p7.setLayout(new BorderLayout());
		p8.setLayout(new BoxLayout(p8, BoxLayout.Y_AXIS));
		p9.setLayout(new BorderLayout());
		p10.setLayout(new BoxLayout(p10, BoxLayout.Y_AXIS));
		p11.setLayout(new BorderLayout(0, 0));		
		p12.setLayout(new BoxLayout(p12, BoxLayout.Y_AXIS));
		
		//레시피이름 정렬/크기 설정
		lr1.setHorizontalAlignment(SwingConstants.CENTER);
		lr1.setPreferredSize(new Dimension(185, 30));
		lr2.setHorizontalAlignment(SwingConstants.CENTER);
		lr2.setPreferredSize(new Dimension(185, 30));		
		lr3.setHorizontalAlignment(SwingConstants.CENTER);
		lr3.setPreferredSize(new Dimension(185, 30));
		//추천수/조회수 크기설정
		lr4.setPreferredSize(new Dimension(70, 10));
		lr5.setPreferredSize(new Dimension(70, 10));
		lr6.setPreferredSize(new Dimension(70, 10));
		lr7.setPreferredSize(new Dimension(70, 10));		
		lr8.setPreferredSize(new Dimension(70, 10));		
		lr9.setPreferredSize(new Dimension(70, 10));
		
		
		btn1.setPreferredSize(new Dimension(260, 150));
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btn2.setPreferredSize(new Dimension(260, 150));		
		btn3.setPreferredSize(new Dimension(260, 150));

		/////////////////////////////////////////////추가
		add(p1, BorderLayout.NORTH);
		p1.add(btn);
		p1.add(lup2);
		add(p2, BorderLayout.CENTER);
		p2.add(scrollPane, BorderLayout.CENTER);
		p3.add(p4);
		p4.add(btn1);
		p4.add(p7);
		p7.add(lr1, BorderLayout.CENTER);
		p7.add(p8, BorderLayout.EAST);
		p8.add(lr4);
		p8.add(lr5);
		p3.add(p5);
		p5.add(btn2);
		p5.add(p9);
		p9.add(lr2, BorderLayout.CENTER);		
		p9.add(p10, BorderLayout.EAST);
		p10.add(lr6);
		p10.add(lr7);
		p3.add(p6);
		p6.add(btn3);
		p6.add(p11);
		p11.add(lr3, BorderLayout.CENTER);
		p11.add(p12, BorderLayout.EAST);
		p12.add(lr8);
		p12.add(lr9);
		
		//버튼 이벤트
		btn.addActionListener((e)->{
			
		});
		
	}
}

