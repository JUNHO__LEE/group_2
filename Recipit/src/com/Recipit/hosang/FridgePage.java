package com.Recipit.hosang;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

class FridgeBackground extends JPanel{					// 배경 그리기
	Image bg = new ImageIcon("images/틀2.PNG").getImage();
	public FridgeBackground(){
		this.setSize(300,600);
		this.setLayout(new BorderLayout());
	}
	@Override
	public void paintComponent(Graphics g) {
		g.drawImage(bg, 0, 1, getWidth(), getHeight(), this);
	}
}

class FridgePanel extends JPanel{					// 화면구성 패널
	JLabel l1, l2, l3, l4;
	JButton b1, b2;
	JPanel p1, p2, p6;
	
	public FridgePanel(FridgePage fp) {
		setOpaque(false);
		setLayout(new BorderLayout());
		
		//-------------------- 라벨 및 버튼 설정------------------------//
		l1 = new JLabel("Fridge(냉장고)", JLabel.CENTER);
		l1.setFont(new Font("돋움", Font.BOLD, 25));
		l1.setForeground(Color.WHITE);
		l1.setPreferredSize(new Dimension(280, 55));			
		
		b1 = new JButton("닫기");
		b1.setPreferredSize(new Dimension(120,30));		// 버튼 크기 설정
		b1.setBackground(Color.RED);					// 버튼 색깔 설정
		b1.setFont(new Font("돋움", Font.BOLD, 20));		// 라벨 폰트 설정
		b1.setForeground(Color.WHITE);					// 라벨 색깔 설정
		b1.setFocusPainted(false);
		
		b2 = new JButton("추가");
		b2.setPreferredSize(new Dimension(120,30));		// 버튼 크기 설정
		b2.setBackground(new Color(0, 0, 65));			// 버튼 색깔 설정
		b2.setFont(new Font("돋움", Font.BOLD, 20));		// 라벨 폰트 설정
		b2.setForeground(Color.WHITE);					// 라벨 색깔 설정
		b2.setFocusPainted(false);
		/////////////////////////////////////////////////////////////////////////
		
		//----------------------------p1 설정-----------------------------------//
		p1 = new JPanel();
		p1.setOpaque(false);
		p1.setLayout(new GridLayout(3,1,0,5));
		JPanel p3 = new RefigeratorRoom();
		JPanel p4 = new Icebox();
		JPanel p5 = new Roomtemp();
		
		p1.add(p3);
		p1.add(p4);
		p1.add(p5);
		p1.setSize(280, 400);
		/////////////////////////////////////////////////////////////////////////
		
		//----------------------------p2 설정-----------------------------------//
		p2 = new JPanel();
		p2.setOpaque(false);
		p2.add(b1);
		p2.add(b2);
		/////////////////////////////////////////////////////////////////////////
		
		//-----------------------b1 이벤트 설정----------------------------------//
		b1.addActionListener((e)->{
			fp.dispose();
		});
		/////////////////////////////////////////////////////////////////////////
		
		//-----------------------b2 이벤트 설정----------------------------------//
		b2.addActionListener((e)->{
			new AddIngrePage();
			fp.dispose();
		});
		
		add(l1, BorderLayout.NORTH);
		add(p1);
		add(p2, BorderLayout.SOUTH);
	}
	
}
public class FridgePage extends JFrame{
	JDialog d;
	public FridgePage() {
		JPanel p1 = new FridgeBackground();
		p1.add(new FridgePanel(this));
		d = new JDialog(this, "", true);
		d.add(p1);
		d.setBounds(810, 200, 300, 600);
		d.setUndecorated(true);			//window 테이블 안보이게
		d.setVisible(true);
		
	}
	public static void main(String[] args) {
		new FridgePage();
	}
}
