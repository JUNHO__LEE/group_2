package com.Recipit.hosang;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JPanel;

public class AddButtonPanel extends JPanel{
	JButton b1, b2, b3;
	Dimension d = new Dimension(300, 35);
	Font f = new Font("돋움", Font.BOLD, 20);
	
	public AddButtonPanel(AddIngrePage ap) {
		setBackground(Color.WHITE);
		//----------------------버튼 설정----------------------//
		b1 = new JButton("냉장실로 이동");
		b1.setBackground(Color.BLUE);
		b1.setFont(f);
		b1.setForeground(Color.WHITE);
		b1.setPreferredSize(d);
		
		b2 = new JButton("냉동실로 이동");
		b2.setBackground(Color.BLUE);
		b2.setFont(f);
		b2.setForeground(Color.WHITE);
		b2.setPreferredSize(d);
		
		b3 = new JButton("상온으로 이동");
		b3.setBackground(Color.BLUE);
		b3.setFont(f);
		b3.setForeground(Color.WHITE);
		b3.setPreferredSize(d);
		///////////////////////////////////////////////////////
		
		//--------------------버튼 이벤트 설정------------------//
		b1.addActionListener((e)->{
			new FridgePage();
			ap.dispose();
		});
		
		b2.addActionListener((e)->{
			new FridgePage();
			ap.dispose();
		});
		
		b3.addActionListener((e)->{
			new FridgePage();
			ap.dispose();
		});
		///////////////////////////////////////////////////////
		
		add(b1);
		add(b2);
		add(b3);
	}
}
