package com.Recipit.hosang;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.tools.DiagnosticCollector;

import javafx.scene.control.Dialog;
import jdk.nashorn.internal.scripts.JD;

class IngrePanelBackground extends JPanel{
	Image bg = new ImageIcon("images/틀3.png").getImage();
	public IngrePanelBackground() {
		this.setSize(300, 550);
		this.setLayout(new BorderLayout());
	}
	@Override
	protected void paintComponent(Graphics g) {
		g.drawImage(bg, 0, 1, getWidth(),getHeight(),this);
	}
}

class AddIngrePanel extends JPanel{
	JLabel l1,l2;
	JPanel p1,p2,p3,p4;
	JButton b1;
	public AddIngrePanel(AddIngrePage ap) {
		setOpaque(false);
		setLayout(new BorderLayout());
		//-------------------- 라벨 및 버튼 설정------------------------//
		l1 = new JLabel("갈 치", JLabel.CENTER);
		l1.setFont(new Font("돋움", Font.BOLD, 25));
		l1.setForeground(Color.WHITE);
		l1.setPreferredSize(new Dimension(280, 55));
		
		l2 = new JLabel(new ImageIcon("images/갈치.jpg"), JLabel.CENTER);
		
		b1 = new JButton("닫기");
		b1.setBackground(Color.BLUE);					// 버튼 색깔 설정
		b1.setFont(new Font("돋움", Font.BOLD, 20));		// 라벨 폰트 설정
		b1.setForeground(Color.WHITE);					// 라벨 색깔 설정
		
		///////////////////////////////////////////////////////////////
		
		//-----------------------p1 패널 설정--------------------------//
		p1 = new JPanel();
		p1.setOpaque(false);
		p1.setLayout(new GridLayout(4,1,0,5));
		
		p2 = new IngreInfoPanel();
		p3 = new ExpirationDatePanel();
		p4 = new AddButtonPanel(ap);
		
		p1.add(l2);
		p1.add(p2);
		p1.add(p3);
		p1.add(p4);
		p1.setSize(280, 400);
		///////////////////////////////////////////////////////////////
		
		//----------------------b1 이벤트 설정-------------------------//
		b1.addActionListener((e)->{
			ap.dispose();
		});
		///////////////////////////////////////////////////////////////
		
		add(l1, BorderLayout.NORTH);
		add(p1);
		add(b1, BorderLayout.SOUTH);
	}
}
public class AddIngrePage extends JFrame{
	JDialog d;
	public AddIngrePage() {
		setUndecorated(true);
		JPanel p1 = new IngrePanelBackground();
		p1.add(new AddIngrePanel(this));
		d = new JDialog(this, "", true);
		d.add(p1);
		d.setBounds(810, 200, 300, 600);
		d.setUndecorated(true);	
		d.setVisible(true);
	}
	public static void main(String[] args) {
		new AddIngrePage();
	}
}
