package com.Recipit.hosang;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

import com.Recipit.master.MyTabbedPaneUI;

import com.Recipit.Kim.Main2_01;
import com.Recipit.kgm.Theme_01;
import com.Recipit.master.*;

class MainJTabbed extends JTabbedPane{

	public SubTabbedFoodRecommenderPane main1;
	public Main2_01 main2;
	public Theme_01 main3;
	public MainJTabbed() {
		this.setUI(new MyTabbedPaneUI(this));
		main1 = new SubTabbedFoodRecommenderPane("메인반찬", "국/탕/찌개", "다이어트", "아이메뉴");
		main2 = new Main2_01();
		main3 = new Theme_01();
		
		this.addTab("오늘추천", main1);
		this.add("식단추천", main2);
		this.addTab("레시피", main3);
	}
}

public class MainPage extends JFrame{
	public MainPage() {
		
		MainJTabbed mTab = new MainJTabbed();
		MainMenubarPanel mmp = new MainMenubarPanel(this);
		
		add(mmp, BorderLayout.NORTH);
		add(mTab);
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(300,600);
		setLocation(500, 200);
		setResizable(false);
		setVisible(true);
	}
	public static void main(String[] args) {
		new MainPage();
	}
}
