


package com.Recipit.hosang;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.Recipit.Lee.Menu;

public class MainMenubarPanel extends JPanel{
	JButton b1, b2, b3;
	JLabel l1;
	
	public MainMenubarPanel(MainPage mp) {
		
		///////////////////////////////////////////////////////////////////////
		l1 = new JLabel("RECIPIT", JLabel.RIGHT);
		l1.setPreferredSize(new Dimension(160, 30));
		l1.setFont(new Font("돋움", Font.BOLD, 30));	// 라벨 폰트 설정
		l1.setForeground(Color.WHITE);
		///////////////////////////////////////////////////////////////////////
		
		b1 = new JButton(new ImageIcon("images/mypage.png"));
		b1.setPreferredSize(new Dimension(40, 40));	

		b1.setFocusPainted(false);
		b1.setContentAreaFilled(false);

		b1.addActionListener((e)->{
			new Menu();
		});

		//////////////////////////////////////////////////////////////////////
		b2 = new JButton(new ImageIcon("images/검색.png"));
		b2.setPreferredSize(new Dimension(30, 30));
		b2.addActionListener((e)->{
			new SearchPage(mp);
		});
		//////////////////////////////////////////////////////////////////////
		b3 = new JButton(new ImageIcon("images/fridge.PNG"));
		b3.setPreferredSize(new Dimension(30, 30));
		b3.addActionListener((e)->{
			new FridgePage();
		});
		//////////////////////////////////////////////////////////////////////
		add(b1);
		add(l1);
		add(b3);
		add(b2);
		
		this.setBackground(Color.BLUE);
	}
}

