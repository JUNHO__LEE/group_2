package com.Recipit.hosang;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

class SearchPanel extends JPanel{
	JLabel l1;
	JPanel p1,p2,p3;
	JButton b1;
	Color c = new Color(180, 255, 180);
	LineBorder bb = new LineBorder(Color.BLACK, 1);
	
	public SearchPanel(SearchPage sp) {
		setLayout(new BorderLayout());
		setBackground(c);
		setBorder(bb);
		//-----------------------p1 설정--------------------------//
		p1 = new JPanel();
		p1.setLayout(new BorderLayout());
		p1.setBackground(Color.WHITE);
		p1.setBorder(bb);
		
		p2 = new SearchTextField();
		p3 = new ResultPanel();
		
		p1.add(p2, BorderLayout.NORTH);
		p1.add(p3);
		///////////////////////////////////////////////////////////
		
		//-----------------------l1 설정--------------------------//
		l1 = new JLabel("검  색", JLabel.CENTER);
		l1.setOpaque(true);
		l1.setBorder(bb);
		l1.setBackground(c);
		l1.setFont(new Font("돋움", Font.BOLD, 25));
		///////////////////////////////////////////////////////////
		
		//-----------------------b1 설정--------------------------//
		b1 = new JButton("닫기");
		b1.setBackground(Color.GREEN);					// 버튼 색깔 설정
		b1.setFont(new Font("돋움", Font.BOLD, 20));		// 라벨 폰트 설정
		b1.setForeground(Color.WHITE);					// 라벨 색깔 설정
		b1.setBorder(bb);
		b1.addActionListener((e)->{
			sp.dispose();
		});
		///////////////////////////////////////////////////////////
		add(l1, BorderLayout.NORTH);
		add(p1);
		add(b1, BorderLayout.SOUTH);
	}
}

public class SearchPage extends JDialog{
	public SearchPage(JFrame p) {
		super(p, "", true);
		this.add(new SearchPanel(this));
		setBounds(810, 200, 300, 600);
		setUndecorated(true);
		setVisible(true);
	}
}
