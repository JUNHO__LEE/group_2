package com.Recipit.hosang;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

public class StartPage extends JFrame {
	JPanel p1;
	JPanel p2;
	JPanel p3;
	ImageIcon icon;
	ImageIcon icon2;
	JButton button;
	JButton button2;
	JTextField tf;
	JPasswordField pf;
	JLabel jl;
	JLabel jl2;
	JLabel jl3;
	JLabel jl4;
	FlowLayout fl = new FlowLayout(FlowLayout.CENTER, 30, 10);
	
	public StartPage() {
		setDefaultCloseOperation(3);
		Container cp = getContentPane();
		cp.setBackground(new Color(50, 60, 255));
		cp.setLayout(fl);
		
		button = new JButton("로그인");
		button.setPreferredSize(new Dimension(250,50));		// 버튼 크기 설정
		button.setBackground(Color.DARK_GRAY);				// 버튼 색깔 설정
		button.setFont(new Font("돋움", Font.PLAIN, 30));	// 라벨 폰트 설정
		button.setForeground(Color.WHITE);					// 라벨 색깔 설정
		button.setFocusPainted(false);						// 클릭시 라벨 테두리 끄기
		//------------------------로그인 버튼 이벤트-----------------------------//
		button.addActionListener((e)->{
			new MainPage();
			this.dispose();
		});
		//----------------------------------------------------------------------//
		
		LineBorder bb = new LineBorder(Color.WHITE, 3);
		button2 = new JButton("회원가입");
		button2.setPreferredSize(new Dimension(250,50));		// 버튼 크기 설정
		button2.setBackground(new Color(50, 60, 255));	// 버튼 색깔 설정
		button2.setBorder(bb);
		button2.setFont(new Font("돋움", Font.PLAIN, 30));	// 라벨 폰트 설정
		button2.setForeground(Color.WHITE);					// 라벨 색깔 설정
		button2.setFocusPainted(false);	
		
		//------------------------회원가입 버튼 이벤트-----------------------------//
		button2.addActionListener((e)->{
			SignupPage sup = new SignupPage(this);
		});
		//----------------------------------------------------------------------//
		
		tf = new JTextField("ID");
		tf.setPreferredSize(new Dimension(250,30));
		
		pf = new JPasswordField("PW");
		pf.setPreferredSize(new Dimension(250,30));
		
		icon = new ImageIcon("images/시작.jpg");
		jl = new JLabel(icon, JLabel.CENTER);
		
		p1 = new JPanel();
		p1.setBackground(new Color(50, 60, 255));
		p1.setPreferredSize(new Dimension(250,20));
		
		p2 = new JPanel();
		p2.setBackground(new Color(50, 60, 255));
		p2.setPreferredSize(new Dimension(250,10));
		
		p3 = new JPanel();
		p3.setBackground(new Color(50, 60, 255));
		p3.setPreferredSize(new Dimension(250,20));
		
		jl2 = new JLabel("RECIPIT", JLabel.CENTER);
		jl2.setFont(new Font("돋움", Font.PLAIN, 30));
		jl2.setForeground(Color.WHITE);
		jl2.setPreferredSize(new Dimension(200,50));

		jl3 = new JLabel("아이디 찾기", JLabel.CENTER);
		jl3.setFont(new Font("돋움", Font.PLAIN, 12));
		jl3.setForeground(Color.CYAN);
		jl3.setPreferredSize(new Dimension(80,20));
		
		jl4 = new JLabel("비밀번호 찾기", JLabel.CENTER);
		jl4.setFont(new Font("돋움", Font.PLAIN, 12));
		jl4.setForeground(Color.CYAN);
		jl4.setPreferredSize(new Dimension(80,20));
		
		cp.add(p1);
		cp.add(jl);
		cp.add(p2);
		cp.add(jl2);
		cp.add(tf);
		cp.add(pf);
		cp.add(button);
		cp.add(button2);
		cp.add(p3);
		cp.add(jl3);
		cp.add(jl4);
		
		setBounds(500, 200, 300, 600);
		setResizable(false);
		setVisible(true);
	}
	
	public static void main(String[] args) {
		new StartPage();
	}
}

