package com.Recipit.hosang;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class IngreInfoLabelPanel extends JPanel{
	JLabel l1, l2;
	Font f = new Font("돋움", Font.BOLD, 20);
	Dimension d = new Dimension(300, 35);
	public IngreInfoLabelPanel() {
		
	//-----------------------p1 라벨 설정---------------------------//
	l1 = new JLabel("카테고리 1", JLabel.RIGHT);
	l1.setFont(f);
	l1.setPreferredSize(d);
	l1.setOpaque(true);
	l1.setBackground(Color.PINK);
	
	l2 = new JLabel("카테고리 2", JLabel.RIGHT);
	l2.setFont(f);
	l2.setPreferredSize(d);
	l2.setOpaque(true);
	l2.setBackground(Color.PINK);
	///////////////////////////////////////////////////////////////
	
	setBackground(Color.WHITE);
	add(l1);
	add(l2);
	}
}
