
-- tablespace 생성 전 삭제
drop tablespace recipit_DB including contents and datafiles;

-- tablespace 생성
create tablespace recipit_DB
datafile 'C:/oraclexe/app/oracle/oradata/XE/RECIPIT_DB.dbf'
size 200m
autoextend on next 10m
maxsize 500m;

-- tablespace datafile 확인
-- select * from dba_data_files where tablespace_name='recipit_DB';

-- 유저 삭제 후 생성
drop user recipit_dba cascade;

create user recipit_dba identified by dba123
default tablespace recipit_DB quota unlimited on recipit_DB
temporary tablespace temp;

-- 유저에게 DBA 권한 주기
grant connect, resource, dba to recipit_dba;

-- 접속
conn recipit_dba/dba123

-- 1. 회원 table 생성
create sequence users_seq start with 1 increment by 1;
create table users(
	num number(8) constraint users_pk_num primary key,
	email varchar2(320) unique,
	pw varchar2(15) not null,
	name varchar2(20) not null,
	phonenum varchar2(22) not null,
	imageadd varchar2(30),
	age number(8),
	job varchar2(60),
	intro varchar2(300)
);

-- 2. 재료(ingre) table 생성
create sequence ingre_seq start with 1 increment by 1;
create table ingre(
	num number(8) constraint ingre_pk_num primary key,
	name varchar2(20) unique,
	category varchar2(20) not null,
	category varchar2(20),
	imageadd varchar2(30),
	text varchar2(300)
);

-- 3. 테마(theme) table 생성
create sequence theme_seq start with 1 increment by 1;
create table theme(
	num number(8) constraint theme_pk_num primary key,
	name varchar2(20) unique,
	category varchar2(20) 
);

-- 4. 레시피(recipe) table 생성
create sequence recipe_seq start with 1 increment by 1;
create table recipe(
	num number(8) constraint recipe_pk_num primary key,
	name varchar2(20) unique,
	theme_name varchar2(20) constraint recipe_fk_theme_name references theme(name) not null,
	users_email varchar2(320)  constraint fridge_fk_users_email references users(email) not null,
	cooktime number(8) not null,
	cal number(8) not null,
	level number(1) not null,
	hit number(8),
	likes number(8),
	imageadd varchar2(30),
	text varchar2(200),
	updatetime timestamp
);

-- 5. 냉장고(fridge) table 생성
create sequence fridge_seq start with 1 increment by 1;
create table fridge(
	num number(8) constraint fridge_pk_num primary key,
	users_email varchar2(320)  constraint fridge_fk_users_email references users(email) not null,
	recipe_name varchar2(20) constraint fridge_fk_recipe_name references recipe(name) not null,
	ingre_name varchar2(20) constraint fridge_fk_ingre_name references ingre(name) not null,
	quantity number(8) not null,
	ismemo number(1) not null,
	isfreezing number(1) not null,
	expirationdate timestamp,
	indate timestamp,
	current_time timestamp 
);

-- 6. 조리과정(proceed) table 생성
create sequence proceed_seq start with 1 increment by 1;
create table proceed(
	num number(8) constraint proceed_pk_num primary key,
	recipe_name varchar2(20) constraint fridge_fk_recipe_name references recipe(name) not null,
	imageadd varchar2(30),
	text varchar2(600)
);

-- 7. 레시피 재료(recipe_ingre) table 생성
create sequence recipe_ingre_seq start with 1 increment by 1;
create table proceed(
	num number(8) constraint recipe_ingre_pk_num primary key,
	recipe_name varchar2(20) constraint fridge_fk_recipe_name references recipe(name) not null,
	ingre_name varchar2(20) constraint fridge_fk_ingre_name references ingre(name) not null,
	quantity number(8),
	ismain number(1),
	servings number(1)
);

-- 8. 북마크(bookmark) table 생성
create sequence bookmark_seq start with 1 increment by 1;
create table bookmark(
	num number(8) constraint bookmark_pk_num primary key,
	recipe_name varchar2(20) constraint bookmark_fk_recipe_name references recipe(name) not null,
	users_email varchar2(320)  constraint bookmark_fk_users_email references users(email) not null,
	indate timestamp
);

-- 9. 댓글(review) table 생성
create sequence review_seq start with 1 increment by 1;
create table review(
	num number(8) constraint review_pk_num primary key,
	recipe_name varchar2(20) constraint review_fk_recipe_name references recipe(name) not null,
	users_email varchar2(320)  constraint review_fk_users_email references users(email) not null,
	text varchar2(300),
	indate timestamp
);

-- 10. 한끼 식단(menu) table 생성
create sequence menu_seq start with 1 increment by 1;
create table menu(
	num number(8) constraint menu_pk_num primary key,
	name varchar2(20) not null,
	recipe_name varchar2(20) constraint menu_fk_recipe_name references recipe(name) not null
);

-- 11. 하루 식단(daymenu) table 생성
create sequence daymenu_seq start with 1 increment by 1;
create table daymenu(
	num number(8) constraint daymenu_pk_num primary key,
	name varchar2(20) not null,
	menu_name varchar2(20) constraint daymenu_fk_menu_name references menu(name) not null
);

-- 12. 일주일 식단(weekmenu) table 생성
create sequence weekmenu_seq start with 1 increment by 1;
create table weekmenu(
	num number(8) constraint weekmenu_pk_num primary key,
	name varchar2(20) not null,
	daymenu_name varchar2(20) constraint weekmenu_fk_daymenu_name references daymenu(name) not null,
	users_email varchar2(320)  constraint weekmenu_fk_users_email references users(email) not null,
	imageadd varchar2(30),
	textbox varchar2(300)
);