package com.Recipit.hosang;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class ExpirationDatePanel extends JPanel{
	JPanel p1, p2, p3, p4;
	JRadioButton r1, r2;
	JComboBox cb1, cb2, cb3, cb4;
	JLabel l1,l2,l3,l4;
	CardLayout card = new CardLayout();;
	Font f = new Font("돋움", Font.BOLD, 15);
	public ExpirationDatePanel() {
		setBackground(Color.WHITE);
	
		//----------------------------라디오 버튼 설정---------------------------------//
		r1 = new JRadioButton("유통기한", true);
		r1.setFont(new Font("돋움", Font.BOLD, 20));
		r1.setBackground(Color.WHITE);
		
		r2 = new JRadioButton("보관일수");
		r2.setFont(new Font("돋움", Font.BOLD, 20));
		r2.setBackground(Color.WHITE);
		
		ButtonGroup buttonGroup = new ButtonGroup();
		buttonGroup.add(r1); buttonGroup.add(r2);
		
		card = JRadioButtonEvent(r1);
		card = JRadioButtonEvent(r2);
		///////////////////////////////////////////////////////////////////////////////
		
		//------------------------유통기한 콤보박스 설정---------------------------------//
		String[] year = {"2017","2018","2019","2020","2021","2022","2013","2014"};
		String[] month = {"1","2","3","4","5","6","7","8","9","10","11","12"};
		String[] day = {"1","2","3","4","5","6","7","8","9","10",
				"11","12","13","14","15","16","17","18","19","20",
				"21","22","23","24","25","26","27","28","29","30","31"};
		
		cb1 = new JComboBox(year);
		cb1.setFont(f);
		cb2 = new JComboBox(month);
		cb2.setFont(f);
		cb3 = new JComboBox(day);
		cb3.setFont(f);
		///////////////////////////////////////////////////////////////////////////////
		
		//--------------------------------p1 패널 설정---------------------------------//
		p1 = new JPanel();
		p1.setBackground(Color.WHITE);
		p1.add(r1);
		p1.add(r2);
		///////////////////////////////////////////////////////////////////////////////
		
		//--------------------------------p3 패널 설정---------------------------------//
		p3 = new JPanel();
		p3.setBackground(Color.WHITE);
		
		l1 = new JLabel("년 ");
		l1.setFont(f);
		l2 = new JLabel("월 ");
		l2.setFont(f);
		l3 = new JLabel(" 일 까지");
		l3.setFont(f);
		
		p3.add(cb1);
		p3.add(l1);
		p3.add(cb2);
		p3.add(l2);
		p3.add(cb3);
		p3.add(l3);
		///////////////////////////////////////////////////////////////////////////////
		
		//--------------------------------p4 패널 설정---------------------------------//
		p4 = new JPanel();
		p4.setBackground(Color.WHITE);
		
		l4 = new JLabel("  일 후 까지");
		l4.setFont(f);
		
		String[] afterday = {"1","2","3","4","5","6","7","8","9","10",
							"11","12","13","14","15","16","17","18","19","20",
							"21","22","23","24","25","26","27","28","29","30",
							"31","32","33","34","35","36","37","38","39","40",
							"41","42","43","44","45","46","47","48","49","50",
							"51","52","53","54","55","56","57","58","59","50",
							"61","62","63","64","65","66","67","68","69","60",
							"71","72","73","74","75","76","77","78","79","80",
							"81","82","83","84","85","86","87","88","89","90",
							"91","92","93","94","95","96","97","98","99"};
		cb4 = new JComboBox(afterday);
		cb4.setFont(f);
		p4.add(cb4);
		p4.add(l4);
		///////////////////////////////////////////////////////////////////////////////

		//--------------------------------p2 패널 설정---------------------------------//
		p2 = new JPanel();
		p2.setLayout(card);
		p2.setSize(280, 50);
		
		p2.add(p3, "유통기한");
		p2.add(p4, "보관일수");
		p2.setBackground(Color.WHITE);
		///////////////////////////////////////////////////////////////////////////////
		add(p1);
		add(p2);
	}
	public CardLayout JRadioButtonEvent(JRadioButton r) {
		r.addActionListener((e)->{
			card.show(p2, e.getActionCommand());
		});
		return card;
	}
}

