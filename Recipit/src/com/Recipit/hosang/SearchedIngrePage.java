package com.Recipit.hosang;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

class SearchedIngrePanel extends JPanel{
	JLabel l1,l2;
	JPanel p1,p2,p3,p4;
	JButton b1;
	public SearchedIngrePanel(SearchedIngrePage sip) {
		setOpaque(false);
		setLayout(new BorderLayout());
		//-------------------- 라벨 및 버튼 설정------------------------//
		l1 = new JLabel("갈 치", JLabel.CENTER);
		l1.setFont(new Font("돋움", Font.BOLD, 25));
		l1.setForeground(Color.WHITE);
		l1.setPreferredSize(new Dimension(280, 55));
		
		l2 = new JLabel(new ImageIcon("images/갈치.jpg"), JLabel.CENTER);
		
		b1 = new JButton("닫기");
		b1.setBackground(Color.BLUE);					// 버튼 색깔 설정
		b1.setFont(new Font("돋움", Font.BOLD, 20));		// 라벨 폰트 설정
		b1.setForeground(Color.WHITE);					// 라벨 색깔 설정
		
		///////////////////////////////////////////////////////////////
		
		//-----------------------p1 패널 설정--------------------------//
		p1 = new JPanel();
		p1.setOpaque(false);
		p1.setLayout(new GridLayout(4,0,0,20));
		
		p4 = new IngreInfoLabelPanel();
		p2 = new IngreTextInfo();
		p3 = new IngreButtonPanel(sip);
		
		p1.add(l2);
		p1.add(p4);
		p1.add(p2);
		p1.add(p3);
		
		p1.setSize(280, 400);
		///////////////////////////////////////////////////////////////
		
		//----------------------b1 이벤트 설정-------------------------//
		b1.addActionListener((e)->{
			sip.dispose();
		});
		///////////////////////////////////////////////////////////////
		
		add(l1, BorderLayout.NORTH);
		add(p1);
		add(b1, BorderLayout.SOUTH);
	}
}

public class SearchedIngrePage extends JDialog{

	public SearchedIngrePage(JFrame f) {
		super(f, "", true);
		setUndecorated(true);
		JPanel p1 = new IngrePanelBackground();
		p1.add(new SearchedIngrePanel(this));
		add(p1);
		setBounds(810, 200, 300, 500);
		setUndecorated(true);	
		setVisible(true);
	}
}
