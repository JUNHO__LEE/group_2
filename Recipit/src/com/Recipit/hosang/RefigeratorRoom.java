package com.Recipit.hosang;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class RefigeratorRoom extends JPanel{
	JPanel p1;
	JButton b1, b2, b3, b4, b5, b6, b7, b8;
	JLabel l1;
	JScrollPane scroll;
	public RefigeratorRoom() {
		setOpaque(false);
		setLayout(new FlowLayout());
		
		l1 = new JLabel("냉장실", JLabel.CENTER);
		l1.setFont(new Font("돋움", Font.BOLD, 20));
		l1.setOpaque(true);
		l1.setBackground(Color.BLUE);
		l1.setForeground(Color.WHITE);
		l1.setPreferredSize(new Dimension(250, 30));
		
		b1 = new JButton(new ImageIcon("images/김치찌개.jpg"));
		b1.setPreferredSize(new Dimension(50, 37));	
		
		b2 = new JButton(new ImageIcon("images/양파.jpg"));
		b2.setPreferredSize(new Dimension(50, 37));
		
		b3 = new JButton(new ImageIcon("images/삼겹살.jpg"));
		b3.setPreferredSize(new Dimension(50, 37));
		
		b4 = new JButton(new ImageIcon("images/두부.jpg"));
		b4.setPreferredSize(new Dimension(50, 37));
		
		b5 = new JButton(new ImageIcon("images/양파.jpg"));
		b5.setPreferredSize(new Dimension(50, 37));
		
		b6 = new JButton(new ImageIcon("images/삼겹살.jpg"));
		b6.setPreferredSize(new Dimension(50, 37));
		
		b7 = new JButton(new ImageIcon("images/두부.jpg"));
		b7.setPreferredSize(new Dimension(50, 37));
		
		p1 = new JPanel();
		p1.setBackground(Color.WHITE);
		
		scroll = new JScrollPane(p1);
		scroll.setPreferredSize(new Dimension(250, 100));
		
		p1.add(b1); p1.add(b2); p1.add(b3);p1.add(b4);p1.add(b5); p1.add(b6);p1.add(b7);	
		
		add(l1);
		add(scroll);
	}
}
