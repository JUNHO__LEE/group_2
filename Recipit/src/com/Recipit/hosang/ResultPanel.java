package com.Recipit.hosang;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class ResultPanel extends JPanel{
	JScrollPane jsp;
	JPanel p1;
	public ResultPanel() {
		setLayout(new BorderLayout());
		p1 = new JPanel();
		p1.setBackground(Color.WHITE);
		
		jsp = new JScrollPane(p1);
		jsp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		add(jsp);
		
	}
}
