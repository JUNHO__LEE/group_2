package com.Recipit.hosang;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JPanel;

public class IngreButtonPanel extends JPanel{
	JButton b1, b2;
	Dimension d = new Dimension(300, 33);
	Font f = new Font("돋움", Font.BOLD, 20);
	public IngreButtonPanel(SearchedIngrePage sip) {
		setBackground(Color.WHITE);
		//--------------------------버튼 설정----------------------------//
		b1 = new JButton("주재료로 사용되는 레시피");
		b1.setBackground(Color.BLUE);
		b1.setFont(f);
		b1.setForeground(Color.WHITE);
		b1.setPreferredSize(d);
		
		b2 = new JButton("냉장고에 추가");
		b2.setBackground(Color.BLUE);
		b2.setFont(f);
		b2.setForeground(Color.WHITE);
		b2.setPreferredSize(d);
		//////////////////////////////////////////////////////////////////
		
		//------------------------버튼 이벤트 설정-------------------------//
		b1.addActionListener((e)->{
			// new 검색창 이동
		});
		
		b2.addActionListener((e)->{
			new FridgePage();
			sip.dispose();
		});
		//////////////////////////////////////////////////////////////////
		add(b1);
		add(b2);
	}
}
