package com.Recipit.hosang;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class IngreTextInfo extends JPanel{
	JPanel p1;
	JTextArea ta;
	public IngreTextInfo() {
		setLayout(new FlowLayout());
		setBackground(Color.WHITE);
		
		//-------------------------ta 설정----------------------------//
		ta = new JTextArea();
		ta.setBounds(0, 0, 300, 80);
		ta.setEditable(false);
		ta.setText("DB에서 가져오자!!!(100자 안으로)");
		///////////////////////////////////////////////////////////////
		
		//------------------------- 패널 설정--------------------------//
		p1 = new JPanel();
		p1.setLayout(new BorderLayout());
		p1.setBackground(Color.white);
		p1.setPreferredSize(new Dimension(300, 80));
		p1.add(ta);
		LineBorder bb = new LineBorder(Color.DARK_GRAY, 2);
		p1.setBorder(bb);
		////////////////////////////////////////////////////////////////

		add(p1);
	}
}
