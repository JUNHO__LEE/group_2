package com.Recipit.hosang;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;


public class SignupPage extends JFrame{
	ImageIcon i1;
	JLabel l1;
	JLabel l2;
	JLabel l3;
	JLabel l4;
	JLabel l5;
	
	JTextField tf1;
	JTextField tf2;
	JTextField tf3;
	JTextField tf4;
	
	JPasswordField pwf1;
	JPasswordField pwf2;
	
	JFrame frame;
	JComboBox cb;
	
	JPanel p1;
	JPanel p2;
	JButton b1;
	JButton b2;
	
	public SignupPage(StartPage sp) {
		
		Container cp = getContentPane();
		cp.setBackground(new Color(50, 60, 255));
		cp.setLayout( new FlowLayout(FlowLayout.CENTER));
		
	
		l1 = new JLabel("회원가입", JLabel.CENTER);
		l1.setPreferredSize(new Dimension(280, 60));
		l1.setFont(new Font("돋움", Font.BOLD, 20));	// 라벨 폰트 설정
		l1.setForeground(Color.WHITE);	
		
		l2 = new JLabel("@");
		l2.setFont(new Font("돋움", Font.BOLD, 30));	// 라벨 폰트 설정
		l2.setForeground(Color.DARK_GRAY);	
		
		tf1 = new JTextField("email 주소(ID)");
		tf1.setPreferredSize(new Dimension(100, 30));
		
		String mail[] = {"gmail","naver", "daum", "yahoo"};
		cb = new JComboBox(mail);
		cb.setPreferredSize(new Dimension(100, 30));
		
		l3 = new JLabel("비밀번호 입력");
		l3.setFont(new Font("돋움", Font.BOLD, 15));	// 라벨 폰트 설정	
		l3.setPreferredSize(new Dimension(100, 20));
		
		l4 = new JLabel("(영/숫자 8자리)");
		l4.setFont(new Font("돋움", Font.BOLD, 10));	
		l4.setPreferredSize(new Dimension(90, 20));
		
		pwf1 = new JPasswordField();
		pwf1.setPreferredSize(new Dimension(190, 30));
		
		l5 = new JLabel("비밀번호 입력");
		l5.setFont(new Font("돋움", Font.BOLD, 15));	// 라벨 폰트 설정	
		l5.setPreferredSize(new Dimension(190, 20));
		
		pwf2 = new JPasswordField();
		pwf2.setPreferredSize(new Dimension(190, 30));
		
		tf2 = new JTextField("이름(닉네임 9자 이내)");
		tf2.setPreferredSize(new Dimension(280, 30));
		
		tf3 = new JTextField("휴대폰 번호 입력(-포함)");
		tf3.setPreferredSize(new Dimension(280, 30));
		
		tf4 = new JTextField("나이");
		tf4.setPreferredSize(new Dimension(280, 30));
		
		//------------------------panel1------------------------------//
		i1 = new ImageIcon("images/틀.PNG");
		
		p1 = new JPanel() {
			public void paintComponent(Graphics g) {
				g.drawImage(i1.getImage(), 5, 10, null);
				setOpaque(false);
				super.paintComponent(g);
			}
		};
		///////////////////////////////////////////////////////////
		
		p1.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		p1.setBackground(new Color(50, 60, 255));
		p1.setPreferredSize(new Dimension(300,350));
		p1.add(l1);
		p1.add(tf1);
		p1.add(l2);
		p1.add(cb);
		p1.add(l3);
		p1.add(l4);
		p1.add(pwf1);
		p1.add(l5);
		p1.add(pwf2);
		p1.add(tf2);
		p1.add(tf3);
		p1.add(tf4);
		
		////////////////////////////////////////////////////////////////
		
		//-------------------------panel2-----------------------------//
		p2 =  new JPanel();
		p2.setLayout(new FlowLayout(FlowLayout.CENTER, 30, 5));
		p2.setBackground(new Color(50, 60, 255));
		p2.setPreferredSize(new Dimension(300,80));
		
		b1 = new JButton("가입 완료");
		b1.setPreferredSize(new Dimension(250,30));		// 버튼 크기 설정
		b1.setBackground(new Color(100, 100, 255));	// 버튼 색깔 설정
		b1.setFont(new Font("돋움", Font.PLAIN, 30));	// 라벨 폰트 설정
		b1.setForeground(Color.WHITE);					// 라벨 색깔 설정
		b1.setFocusPainted(false);			
		
		b1.addActionListener((e)->{
			new MainPage();
			this.dispose();
			sp.dispose();
		});
		
		b2 = new JButton("취 소");
		b2.setPreferredSize(new Dimension(250,30));		// 버튼 크기 설정
		b2.setBackground(new Color(255, 50, 50));		// 버튼 색깔 설정
		b2.setFont(new Font("돋움", Font.PLAIN, 30));	// 라벨 폰트 설정
		b2.setForeground(Color.WHITE);					// 라벨 색깔 설정
		b2.setFocusPainted(false);
		
		b2.addActionListener((e)->{
			this.dispose();
		});
		
		p2.add(b1);
		p2.add(b2);
		//////////////////////////////////////////////////////////////////
		
		cp.add(p1);
		cp.add(p2);
		
		setBounds(500, 200, 300, 600);
		setUndecorated(true);
		setResizable(false);
		setVisible(true);
	}
}

