package com.Recipit.hosang;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class IngreInfoPanel extends JPanel{
	JPanel p1,p2,p3;
	JLabel l1, l2, l3, l4, l5, l6, l7;
	
	public IngreInfoPanel() {
		setLayout(new GridLayout(3,0,0,5));
		setBackground(Color.WHITE);
		
		//-----------------------p1 라벨 설정---------------------------//
		l1 = new JLabel("카테고리 1", JLabel.LEFT);
		l1.setFont(new Font("돋움", Font.BOLD, 20));
		
		l2 = new JLabel("카테고리 2", JLabel.RIGHT);
		l2.setFont(new Font("돋움", Font.BOLD, 20));
		
		l3 = new JLabel("/");
		l3.setFont(new Font("돋움", Font.BOLD, 25));
		///////////////////////////////////////////////////////////////
		
		//-----------------------p2 라벨 설정---------------------------//
		l4 = new JLabel("보유량", JLabel.CENTER);
		l4.setFont(new Font("돋움", Font.BOLD, 20));
		
		l5 = new JLabel("150 g", JLabel.RIGHT);
		l5.setFont(new Font("돋움", Font.BOLD, 20));
		///////////////////////////////////////////////////////////////
		
		//-----------------------p2 라벨 설정---------------------------//
		l6 = new JLabel("냉장고에 넣은 날짜", JLabel.CENTER);
		l6.setFont(new Font("돋움", Font.BOLD, 17));
		
		l7 = new JLabel("2017.10.01", JLabel.RIGHT);
		l7.setFont(new Font("돋움", Font.BOLD, 17));
		///////////////////////////////////////////////////////////////
				
		//------------------------- 패널 설정---------------------------//
		
		p1 = new JPanel();
		p1.setBackground(Color.PINK);
		
		p1.add(l1);
		p1.add(l3);
		p1.add(l2);
		
		p2 = new JPanel();
		p2.setBackground(Color.PINK);
		p2.setLayout(new BorderLayout());
		p2.add(l4, BorderLayout.WEST);
		p2.add(l5);
		
		p3 = new JPanel();
		p3.setBackground(Color.PINK);
		p3.setLayout(new BorderLayout());
		p3.add(l6, BorderLayout.WEST);
		p3.add(l7);
		///////////////////////////////////////////////////////////////
		
		add(p1);
		add(p2);
		add(p3);
	}
}
