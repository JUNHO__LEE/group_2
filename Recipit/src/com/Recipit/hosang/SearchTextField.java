package com.Recipit.hosang;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JTextField;

public class SearchTextField extends JPanel{
	JTextField tf;
	JPanel p1,p2,p3,p4;
	Color c = new Color(50, 255, 50);
	String msg;
	public SearchTextField() {
		setLayout(new BorderLayout());
		tf = new JTextField();
		tf.addActionListener((e)->{
			msg = tf.getText();
			tf.setText("");
		});
		p1 = new JPanel();
		p1.setBackground(c);
		p2 = new JPanel();
		p2.setBackground(c);
		p3 = new JPanel();
		p3.setBackground(c);
		p4 = new JPanel();
		p4.setBackground(c);
		
		add(p1, BorderLayout.NORTH);
		add(p2, BorderLayout.SOUTH);
		add(p3, BorderLayout.EAST);
		add(p4, BorderLayout.WEST);
		add(tf);
		
	}
}
