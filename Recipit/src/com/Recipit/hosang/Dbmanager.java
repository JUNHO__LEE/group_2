package com.Recipit.hosang;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Dbmanager {
	private static String url = "jdbc:oracle:thin:@localhost:1521:xe";
	private static String user = "recipit_dba";
	private static String pw = "dba123";
	private static Connection conn = null;
	
	public static Connection getConnection() throws ClassNotFoundException, SQLException{
		conn = DriverManager.getConnection(url, user, pw);
		return conn;
	}
}
