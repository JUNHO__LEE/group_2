/*********************************************
 *			 	이미지 버튼 이벤트 클래스			 *
 *********************************************
 * @param 			 						 *
 * - 이미지 버튼에 대한 Event를 추가한다.			 *
 *********************************************/

package com.Recipit.master;

import java.awt.Component;
import java.awt.event.*;

public class PicButtonActionListener implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		Component com = (Component)obj;
		
		System.out.println(com.getName());
	}
}