package com.Recipit.master;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.io.IOException;

import javax.swing.JPanel;

/*********************************************
 *			CardLayoutPanel 클래스			 *
 *********************************************
 * @param									 *
 * CardLayout으로 이루어진 패널 반환					 *
 * 											 *
 *********************************************/
@SuppressWarnings("serial")
class CardLayoutPanel extends JPanel {
	private PicPanel c1;
	private PicPanel c2;
	private PicPanel c3;
	private PicPanel c4;
	//JPanel southPanel;
	private CardLayout cardLayout;
	PanelMouseAdapter pma;
	
	public CardLayoutPanel(String imgFileLocation1, String imgFileLocation2, String imgFileLocation3, String imgFileLocation4) {
		cardLayout = new CardLayout();
		
		c1 = new PicPanel(imgFileLocation1, 252, 170);
		c2 = new PicPanel(imgFileLocation2, 252, 170);
		c3 = new PicPanel(imgFileLocation3, 252, 170);
		c4 = new PicPanel(imgFileLocation4, 252, 170);
		pma = new PanelMouseAdapter();
		//southPanel = new JPanel();
		c1.setLayout(new BorderLayout());
		c2.setLayout(new BorderLayout());
		c3.setLayout(new BorderLayout());
		c4.setLayout(new BorderLayout());
		
		c1.setName("c1");
		c2.setName("c2");
		c3.setName("c3");
		c4.setName("c4");
		
		c1.addMouseListener(pma);
		c2.addMouseListener(pma);
		c3.addMouseListener(pma);
		c4.addMouseListener(pma);
		
		/*
		southPanel.add(picBut1);
		southPanel.add(picBut2);
		southPanel.add(picBut3);
		southPanel.add(picBut4);
		*/
		/*c1.add(picBut1);
		c2.add(picBut2);
		c3.add(picBut3);
		c4.add(picBut4);*/
		
		
		this.setLayout(cardLayout);
		//this.addMouseListener(pma);
		
		this.add(c1);
		this.add(c2);
		this.add(c3);
		this.add(c4);
	}

	public CardLayout getCardLayout() {
		return cardLayout;
	}

	public void setCardLayout(CardLayout cardLayout) {
		this.cardLayout = cardLayout;
	}
	
}