
/*********************************************
 *				메뉴 1-1/1-2/1-3 클래스			 *
 *********************************************
 * @param									 *
 * 식단(월화수목금토일) JTabbedPane 반환			 *
 * 											 *
 * @param name1, name2, name3, name4		 *
 * 탭의 이름이 name1, name2, name3, name4인		 *
 * JTabbedPane 반환							 *
 *********************************************/

package com.Recipit.master;

/*import java.awt.Dimension;
import java.awt.Scrollbar;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JToolBar;*/
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

@SuppressWarnings("serial")
public class SubTabbedFoodRecommenderPane extends JPanel {
	private JTabbedPane jtp;	//오늘 추천(메뉴1-1)
	//private JToolBar jtb;		//식단 추천(메뉴2-1)
	private BoxLayoutPanel sideDishes;		//메인반찬
	private BoxLayoutPanel soups;			//국/탕/찌개
	private BoxLayoutPanel dietDishes;		//다이어트
	private BoxLayoutPanel dishesForKids;	//아이메뉴
	private JScrollPane scrollbar1;		//메인반찬용 스크롤바
	private JScrollPane scrollbar2;		//국/탕/찌개용 스크롤바
	private JScrollPane scrollbar3;		//다이어트용 스크롤바
	private JScrollPane scrollbar4;		//아이메뉴용 스크롤바
	
	/*private JPanel defaultPage;		//디폴트 페이지
	private JPanel sunPanel;		//일
	private JPanel monPanel;		//월
	private JPanel tuePanel;		//화
	private JPanel wedPanel;		//수
	private JPanel thuPanel;		//목
	private JPanel friPanel;		//금
	private JPanel satPanel;		//토
*/	//private CardLayout menuCard;	//메뉴2 부분 card layout
	
	/*private JButton sunBut;
	private JButton monBut;
	private JButton tueBut;
	private JButton wedBut;
	private JButton thuBut;
	private JButton friBut;
	private JButton satBut;*/
	
	/*public SubTabbedFoodRecommenderPane() {
		
		*//*********************************************
		 *			메뉴 2-1 버튼 클릭시 탭 변경				 *
		 *********************************************
		 * @param									 *
		 * 식단(월화수목금토일) JTabbedPane 반환			 *
		 * 											 *
		 * @param name1, name2, name3, name4		 *
		 * 탭의 이름이 name1, name2, name3, name4인		 *
		 * JTabbedPane 반환							 *
		 *********************************************//*
		class FoodRecommenderActionListener implements ActionListener {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				switch(e.getActionCommand()) {
				case "일":
					sunBut.setEnabled(false);
					monBut.setEnabled(true);
					tueBut.setEnabled(true);
					wedBut.setEnabled(true);
					thuBut.setEnabled(true);
					friBut.setEnabled(true);
					satBut.setEnabled(true);
					SubTabbedFoodRecommenderPane.this.remove(monPanel);
					SubTabbedFoodRecommenderPane.this.remove(tuePanel);
					SubTabbedFoodRecommenderPane.this.remove(wedPanel);
					SubTabbedFoodRecommenderPane.this.remove(thuPanel);
					SubTabbedFoodRecommenderPane.this.remove(friPanel);
					SubTabbedFoodRecommenderPane.this.remove(satPanel);
					SubTabbedFoodRecommenderPane.this.add(sunPanel, BorderLayout.CENTER);
					break;
				case "월":
					sunBut.setEnabled(true);
					monBut.setEnabled(false);
					tueBut.setEnabled(true);
					wedBut.setEnabled(true);
					thuBut.setEnabled(true);
					friBut.setEnabled(true);
					satBut.setEnabled(true);
					SubTabbedFoodRecommenderPane.this.remove(sunPanel);
					SubTabbedFoodRecommenderPane.this.remove(tuePanel);
					SubTabbedFoodRecommenderPane.this.remove(wedPanel);
					SubTabbedFoodRecommenderPane.this.remove(thuPanel);
					SubTabbedFoodRecommenderPane.this.remove(friPanel);
					SubTabbedFoodRecommenderPane.this.remove(satPanel);
					SubTabbedFoodRecommenderPane.this.add(monPanel, BorderLayout.CENTER);
					break;
				case "화":
					sunBut.setEnabled(true);
					monBut.setEnabled(true);
					tueBut.setEnabled(false);
					wedBut.setEnabled(true);
					thuBut.setEnabled(true);
					friBut.setEnabled(true);
					satBut.setEnabled(true);
					SubTabbedFoodRecommenderPane.this.remove(sunPanel);
					SubTabbedFoodRecommenderPane.this.remove(monPanel);
					SubTabbedFoodRecommenderPane.this.remove(wedPanel);
					SubTabbedFoodRecommenderPane.this.remove(thuPanel);
					SubTabbedFoodRecommenderPane.this.remove(friPanel);
					SubTabbedFoodRecommenderPane.this.remove(satPanel);
					SubTabbedFoodRecommenderPane.this.add(tuePanel, BorderLayout.CENTER);
					break;
				case "수":
					sunBut.setEnabled(true);
					monBut.setEnabled(true);
					tueBut.setEnabled(true);
					wedBut.setEnabled(false);
					thuBut.setEnabled(true);
					friBut.setEnabled(true);
					satBut.setEnabled(true);
					SubTabbedFoodRecommenderPane.this.remove(sunPanel);
					SubTabbedFoodRecommenderPane.this.remove(monPanel);
					SubTabbedFoodRecommenderPane.this.remove(tuePanel);
					SubTabbedFoodRecommenderPane.this.remove(thuPanel);
					SubTabbedFoodRecommenderPane.this.remove(friPanel);
					SubTabbedFoodRecommenderPane.this.remove(satPanel);
					SubTabbedFoodRecommenderPane.this.add(wedPanel, BorderLayout.CENTER);
					break;
				case "목":
					sunBut.setEnabled(true);
					monBut.setEnabled(true);
					tueBut.setEnabled(true);
					wedBut.setEnabled(true);
					thuBut.setEnabled(false);
					friBut.setEnabled(true);
					satBut.setEnabled(true);
					SubTabbedFoodRecommenderPane.this.remove(sunPanel);
					SubTabbedFoodRecommenderPane.this.remove(monPanel);
					SubTabbedFoodRecommenderPane.this.remove(tuePanel);
					SubTabbedFoodRecommenderPane.this.remove(wedPanel);
					SubTabbedFoodRecommenderPane.this.remove(friPanel);
					SubTabbedFoodRecommenderPane.this.remove(satPanel);
					SubTabbedFoodRecommenderPane.this.add(thuPanel, BorderLayout.CENTER);
					break;
				case "금":
					sunBut.setEnabled(true);
					monBut.setEnabled(true);
					tueBut.setEnabled(true);
					wedBut.setEnabled(true);
					thuBut.setEnabled(true);
					friBut.setEnabled(false);
					satBut.setEnabled(true);
					SubTabbedFoodRecommenderPane.this.remove(sunPanel);
					SubTabbedFoodRecommenderPane.this.remove(monPanel);
					SubTabbedFoodRecommenderPane.this.remove(tuePanel);
					SubTabbedFoodRecommenderPane.this.remove(wedPanel);
					SubTabbedFoodRecommenderPane.this.remove(thuPanel);
					SubTabbedFoodRecommenderPane.this.remove(satPanel);
					SubTabbedFoodRecommenderPane.this.add(friPanel, BorderLayout.CENTER);
					break;
				case "토":
					sunBut.setEnabled(true);
					monBut.setEnabled(true);
					tueBut.setEnabled(true);
					wedBut.setEnabled(true);
					thuBut.setEnabled(true);
					friBut.setEnabled(true);
					satBut.setEnabled(false);
					SubTabbedFoodRecommenderPane.this.remove(sunPanel);
					SubTabbedFoodRecommenderPane.this.remove(monPanel);
					SubTabbedFoodRecommenderPane.this.remove(tuePanel);
					SubTabbedFoodRecommenderPane.this.remove(wedPanel);
					SubTabbedFoodRecommenderPane.this.remove(thuPanel);
					SubTabbedFoodRecommenderPane.this.remove(friPanel);
					SubTabbedFoodRecommenderPane.this.add(satPanel, BorderLayout.CENTER);
					break;
				default: System.out.println("버그2");
				}

				SubTabbedFoodRecommenderPane.this.repaint();
				SubTabbedFoodRecommenderPane.this.setVisible(true);
			}
		}
		
		FoodRecommenderActionListener foodListener = new FoodRecommenderActionListener();
		jtb = new JToolBar();
		sunPanel = new JPanel();
		monPanel = new JPanel(); 
		tuePanel = new JPanel();
		wedPanel = new JPanel();
		thuPanel = new JPanel();
		friPanel = new JPanel();
		satPanel = new JPanel();
		sunBut = new JButton("일");
		monBut = new JButton("월");
		tueBut = new JButton("화");
		wedBut = new JButton("수");
		thuBut = new JButton("목");
		friBut = new JButton("금");
		satBut = new JButton("토");
		
		jtb.setFloatable(false);	//툴바 마우스로 못움직이게함
		
		sunBut.addActionListener(foodListener);
		monBut.addActionListener(foodListener);
		tueBut.addActionListener(foodListener);
		wedBut.addActionListener(foodListener);
		thuBut.addActionListener(foodListener);
		friBut.addActionListener(foodListener);
		satBut.addActionListener(foodListener);
		
		jtb.add(sunBut);
		jtb.add(monBut);
		jtb.add(tueBut);
		jtb.add(wedBut);
		jtb.add(thuBut);
		jtb.add(friBut);
		jtb.add(satBut);
		
		this.setLayout(new BorderLayout());
		this.add(jtb, BorderLayout.NORTH);
	}*/
	public SubTabbedFoodRecommenderPane(String tabName1, String tabName2, String tabName3, String tabName4) {
		super();
		
		this.setLayout(new BorderLayout());
		jtp = new JTabbedPane();
		sideDishes = new BoxLayoutPanel(	//메인반찬
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/메인반찬/제육볶음.jpg",
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/메인반찬/닭도리탕.jpg",
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/메인반찬/오징어볶음.jpg",
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/메인반찬/마파두부.jpg"
		);
		soups = new BoxLayoutPanel(		//국/탕/찌개
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/국탕찌개/된장찌개.jpg",
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/국탕찌개/김치찌개.jpg",
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/국탕찌개/순두부찌개.jpg",
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/국탕찌개/버섯전골.jpg"
		);
		dietDishes = new BoxLayoutPanel(	//다이어트
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/다이어트/다이어트1.jpg",
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/다이어트/다이어트2.jpg",
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/다이어트/다이어트3.jpg",
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/다이어트/다이어트4.jpg"
		);
		dishesForKids = new BoxLayoutPanel(//아이메뉴
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/아이식단/아이식단1.jpg",
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/아이식단/아이식단2.jpg",
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/아이식단/아이식단3.jpg",
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/아이식단/아이식단4.jpg"
		);

		scrollbar1 = new JScrollPane(sideDishes);
		scrollbar2 = new JScrollPane(soups);
		scrollbar3 = new JScrollPane(dietDishes);
		scrollbar4 = new JScrollPane(dishesForKids);
		
		scrollbar1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);  
		scrollbar2.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);  
		scrollbar3.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);  
		scrollbar4.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);  
		
		scrollbar1.getVerticalScrollBar().setUnitIncrement(16);		//스크롤바 마우스휠 스피드업
		scrollbar2.getVerticalScrollBar().setUnitIncrement(16);
		scrollbar3.getVerticalScrollBar().setUnitIncrement(16);
		scrollbar4.getVerticalScrollBar().setUnitIncrement(16);
		
		sideDishes.getC1().setText("제육볶음");
		sideDishes.getC2().setText("김치찌개");
		sideDishes.getC3().setText("순두부찌개");
		sideDishes.getC4().setText("마파두부");
		soups.getC1().setText("테스트5");
		soups.getC2().setText("테스트6");
		soups.getC3().setText("테스트7");
		soups.getC4().setText("테스트8");
		dietDishes.getC1().setText("테스트9");
		dietDishes.getC2().setText("테스트10");
		dietDishes.getC3().setText("테스트11");
		dietDishes.getC4().setText("테스트12");
		dishesForKids.getC1().setText("테스트13");
		dishesForKids.getC2().setText("테스트14");
		dishesForKids.getC3().setText("테스트15");
		dishesForKids.getC4().setText("테스트16");
		
		sideDishes.getC1().setName("제육볶음");
		sideDishes.getC2().setName("김치찌개");
		sideDishes.getC3().setName("순두부찌개");
		sideDishes.getC4().setName("마파두부");
		
		jtp.setUI(new IsoscelesTrapezoidTabbedPaneUI());
		
		jtp.addTab(tabName1, scrollbar1);
		jtp.addTab(tabName2, scrollbar2);
		jtp.addTab(tabName3, scrollbar3);
		jtp.addTab(tabName4, scrollbar4);
		this.add(jtp);
	}
	public BoxLayoutPanel getSideDishes() {
		return sideDishes;
	}
	public BoxLayoutPanel getSoups() {
		return soups;
	}
	public BoxLayoutPanel getDietDishes() {
		return dietDishes;
	}
	public BoxLayoutPanel getDishesForKids() {
		return dishesForKids;
	}
	public JTabbedPane getJtp() {
		return jtp;
	}

}