package com.Recipit.master;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

/*********************************************
 *			BoxLayoutPanel 클래스			 	 *
 *********************************************
 * @param									 *
 * BoxLayout으로 이루어진 패널 반환				 *
 * 											 *
 *********************************************/
@SuppressWarnings("serial")
public class BoxLayoutPanel extends JPanel {
	private PicButton c1;
	private PicButton c2;
	private PicButton c3;
	private PicButton c4;
	private BoxLayout boxLayout;
	private JLabel hits;			//조회수
	private JLabel recommend;		//추천수
	private String imgFileLocation = "";
	
	public BoxLayoutPanel() {
		c1 = new PicButton(imgFileLocation, 200, 140);
		hits = new JLabel();
		recommend = new JLabel();
		boxLayout = new BoxLayout(this, BoxLayout.Y_AXIS);
		
		this.setLayout(boxLayout);
		this.add(c1);
	}
	public BoxLayoutPanel(String imgFileLocation1, String imgFileLocation2, String imgFileLocation3, String imgFileLocation4) {
		c1 = new PicButton(imgFileLocation1, 200, 140);
		c2 = new PicButton(imgFileLocation2, 200, 140);
		c3 = new PicButton(imgFileLocation3, 200, 140);
		c4 = new PicButton(imgFileLocation4, 200, 140);
		
		boxLayout = new BoxLayout(this, BoxLayout.Y_AXIS);
		this.setLayout(boxLayout);
		this.add(c1);
		this.add(c2);
		this.add(c3);
		this.add(c4);
	}
	public JLabel getHits() {
		return hits;
	}
	public void setHits(JLabel hits) {
		this.hits = hits;
	}
	public JLabel getRecommend() {
		return recommend;
	}
	public void setRecommend(JLabel recommend) {
		this.recommend = recommend;
	}
	public PicButton getC1() {
		return c1;
	}
	public PicButton getC2() {
		return c2;
	}
	public PicButton getC3() {
		return c3;
	}
	public PicButton getC4() {
		return c4;
	}
	public void setImgFileLocation(String imgFileLocation) {
		this.imgFileLocation = imgFileLocation;
	}
}