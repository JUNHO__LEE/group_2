
package com.Recipit.master;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.JFrame;

/*********************************************
 *				메뉴 1/2/3 클래스				 *
 *********************************************/
/*@SuppressWarnings("serial")
class MainTabbedFoodRecommenderPane extends JTabbedPane {
	private SubTabbedFoodRecommenderPane subTabbed1;	//메뉴 1-1
	private SubTabbedFoodRecommenderPane subTabbed2;	//메뉴 2-1
	private JPanel subTabbed3;	//메뉴3-1(메모)
	private JPanel subPan1;
	private JPanel subPan2;
	private JPanel subPan3;
	
	public MainTabbedFoodRecommenderPane() {
		this.setUI(new MyTabbedPaneUI(this));
		setSubTabbed1(new SubTabbedFoodRecommenderPane("메인반찬", "국/탕/찌개", "다이어트", "아이메뉴"));
		subTabbed2 = new SubTabbedFoodRecommenderPane();
		subTabbed3 = new JPanel();
		subPan1 = new JPanel();
		subPan2 = new JPanel();
		subPan3 = new JPanel();
		subPan1.setLayout(new BorderLayout());
		subPan2.setLayout(new BorderLayout());
		subPan3.setLayout(new BorderLayout());
		
		subPan1.add(new JPanel(), BorderLayout.NORTH);		//여백의미
		subPan1.add(new JPanel(), BorderLayout.WEST);		//여백의미
		subPan1.add(new JPanel(), BorderLayout.EAST);		//여백의미
		subPan2.add(new JPanel(), BorderLayout.NORTH);		//여백의미
		subPan2.add(new JPanel(), BorderLayout.WEST);		//여백의미
		subPan2.add(new JPanel(), BorderLayout.EAST);		//여백의미
		subPan2.add(new JPanel(), BorderLayout.SOUTH);		//여백의미
		subPan3.add(new JPanel(), BorderLayout.NORTH);		//여백의미
		subPan3.add(new JPanel(), BorderLayout.WEST);		//여백의미
		subPan3.add(new JPanel(), BorderLayout.EAST);		//여백의미
		subPan3.add(new JPanel(), BorderLayout.SOUTH);		//여백의미
	
		subTabbed3.setLayout(new GridLayout());
		
		subPan1.add(getSubTabbed1(), BorderLayout.CENTER);
		subPan2.add(subTabbed2, BorderLayout.CENTER);
		subPan3.add(subTabbed3, BorderLayout.CENTER);
		
		this.addTab("오늘 추천", subPan1);
		this.addTab("식단 추천", subPan2);
		this.addTab("메모", subPan3);
		//this.setSelectedIndex(1);
	}

	public SubTabbedFoodRecommenderPane getSubTabbed1() {
		return subTabbed1;
	}

	public void setSubTabbed1(SubTabbedFoodRecommenderPane subTabbed1) {
		this.subTabbed1 = subTabbed1;
	}
}*/

/*********************************************
 *				음식 추천 어플 컨텐츠 뷰어			 *
 *********************************************/
/*@SuppressWarnings("serial")
class FoodRecommenderFrame extends JFrame {
	MainTabbedFoodRecommenderPane main;
	
	public FoodRecommenderFrame() {
		super("레시핏 테스트 v0.1");
		main = new MainTabbedFoodRecommenderPane();
		
		this.add(main);
		this.pack();
		this.setSize(300, 300);
		this.setLocation(600, 300);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		while(true) {
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			switch(main.getSubTabbed1().getJtp().getSelectedIndex()) {
				case 0: 		//메인반찬 탭 선택시 3초마다 다음 CardLayout을 보여줌
					main.getSubTabbed1().getSideDishes().getCardLayout().next(main.getSubTabbed1().getSideDishes()); break;
				case 1: 		//국/탕/찌개 탭 선택시 3초마다 다음 CardLayout을 보여줌
					main.getSubTabbed1().getSoups().getCardLayout().next(main.getSubTabbed1().getSoups()); break;
				case 2: 		//다이어트 탭 선택시 3초마다 다음 CardLayout을 보여줌
					main.getSubTabbed1().getDietDishes().getCardLayout().next(main.getSubTabbed1().getDietDishes()); break;
				case 3: 		//아이메뉴 탭 선택시 3초마다 다음 CardLayout을 보여줌
					main.getSubTabbed1().getDishesForKids().getCardLayout().next(main.getSubTabbed1().getDishesForKids()); break;
				default: System.out.println("버그1");
			}
		}
	}
}*/

/*********************************************
 *					음식 추천 어플				 *
 *********************************************/
/*public class FoodRecommenderApp {

	public static void main(String[] args) {
		//new FoodRecommenderFrame();
	}
}*/

