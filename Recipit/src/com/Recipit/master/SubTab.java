/*********************************************
 *				메뉴 1-1/1-2/1-3 클래스			 *
 *********************************************
 * @param name1, name2, name3, name4		 *
 * 탭의 이름이 name1, name2, name3, name4인		 *
 * JTabbedPane 반환							 *
 *********************************************/

package com.Recipit.master;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;

@SuppressWarnings("serial")
public class SubTab extends JPanel {
	private JTabbedPane jtp;	//오늘 추천(메뉴1-1)
	private CardLayoutPanel sideDishes;		//메인반찬
	private CardLayoutPanel soups;			//국/탕/찌개
	private CardLayoutPanel dietDishes;		//다이어트
	private CardLayoutPanel dishesForKids;	//아이메뉴
	
	public SubTab(String tabName1, String tabName2, String tabName3, String tabName4) {
		super();
		
		this.setLayout(new BorderLayout());
		jtp = new JTabbedPane();
		sideDishes = new CardLayoutPanel(	//메인반찬
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/메인반찬/제육볶음.jpg",
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/메인반찬/닭도리탕.jpg",
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/메인반찬/오징어볶음.jpg",
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/메인반찬/마파두부.jpg"
		);
		soups = new CardLayoutPanel(		//국/탕/찌개
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/국탕찌개/된장찌개.jpg",
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/국탕찌개/김치찌개.jpg",
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/국탕찌개/순두부찌개.jpg",
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/국탕찌개/버섯전골.jpg"
		);
		dietDishes = new CardLayoutPanel(	//다이어트
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/다이어트/다이어트1.jpg",
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/다이어트/다이어트2.jpg",
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/다이어트/다이어트3.jpg",
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/다이어트/다이어트4.jpg"
		);
		dishesForKids = new CardLayoutPanel(//아이메뉴
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/아이식단/아이식단1.jpg",
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/아이식단/아이식단2.jpg",
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/아이식단/아이식단3.jpg",
				System.getenv("userprofile") + "/Documents/java_project/group_2/Recipit/프로젝트1/아이식단/아이식단4.jpg"
		);
		
		jtp.setUI(new IsoscelesTrapezoidTabbedPaneUI());

		jtp.addTab(tabName1, sideDishes);
		jtp.addTab(tabName2, soups);
		jtp.addTab(tabName3, dietDishes);
		jtp.addTab(tabName4, dishesForKids);
		
		this.add(jtp);
	}
	public CardLayoutPanel getSideDishes() {
		return sideDishes;
	}
	public CardLayoutPanel getSoups() {
		return soups;
	}
	public CardLayoutPanel getDietDishes() {
		return dietDishes;
	}
	public CardLayoutPanel getDishesForKids() {
		return dishesForKids;
	}
	public JTabbedPane getJtp() {
		return jtp;
	}
}