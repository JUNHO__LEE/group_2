package com.Recipit.master;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/*********************************************
 *				이미지 리사이징 클래스				 *
 *********************************************
 * @param									 *
 * 이미지의 파일경로, 이미지 가로길이, 이미지 세로길이를 받아	 *
 * 이미지를 재설정하는 클래스						 *
 * 											 *
 * 											 *
 * - getBufferedImage						 *
 * BufferedImage의 형태로						 *
 * 이미지 크기를 재설정하여 리턴하는 메소드				 *
 * 											 *
 * 											 *
 * - saveResizedImageToFile					 *
 * 이미지 크기를 재설정하여 파일로 저장하는 메소드			 *
 *********************************************/
class ImageResizer {
	private BufferedImage img = null;
	private Graphics2D g = null;
	
	public BufferedImage getBufferedImage (String fileName, int width, int height) {
		
		try {
			img = ImageIO.read(new File(fileName));
			//불러온 이미지 파일을 임시저장공간 버퍼에 보관
			BufferedImage thumbnail = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
			//새로쓸 이미지를 임시 저장할 공간 생성
			g = thumbnail.createGraphics();
			g.drawImage(img, 0, 0, width, height, null);
			return img;
		}catch(IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveResizedImageToFile(String fileName, String newFileName, int width, int height) {
		try {
			img = ImageIO.read(new File(fileName));
			BufferedImage thumbnail = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
			g = thumbnail.createGraphics();
			g.drawImage(img, 0, 0, width, height, null);
			boolean isSaveToFile = ImageIO.write(thumbnail, "jpg", new File(newFileName + "_" + System.currentTimeMillis() + ".jpg"));		//저장
			System.out.println(isSaveToFile);
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
}