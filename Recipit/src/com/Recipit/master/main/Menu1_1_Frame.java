package com.Recipit.master.main;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import com.Recipit.Kim.Main2_01;
import com.Recipit.hosang.MainMenubarPanel;
import com.Recipit.kgm.Theme_01;
import com.Recipit.master.SubTabbedFoodRecommenderPane;
import com.Recipit.master.BoxLayoutPanel;
import com.Recipit.master.MyTabbedPaneUI;

public class Menu1_1_Frame extends JFrame {
	public Menu1_1_Frame() {
		MainJTabbed mTab = new MainJTabbed();
		//MainMenubarPanel mmp = new MainMenubarPanel();
		
		mTab.main1.getJtp().setTabComponentAt(1, new BoxLayoutPanel());
		//add(mmp, BorderLayout.NORTH);
		add(mTab);
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(300, 600);
		setLocation(500, 200);
		setResizable(false);
		setVisible(true);
	}
	public static void main(String[] args) {
		new MainPage();
	}
}