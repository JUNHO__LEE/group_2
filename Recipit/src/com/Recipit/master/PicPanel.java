package com.Recipit.master;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;


/*********************************************
 *				이미지 패널 클래스					 *
 *********************************************
 * @param filename, width, height			 *
 * 이미지의 파일경로, 이미지 가로길이, 이미지 세로길이를 받아	 *
 * 패널에 이미지를 출력하는 이미지패널 클래스				 *
 *********************************************/
class PicPanel extends JPanel {
	private ImageResizer ir;
	private BufferedImage img;
	private int width, height;
	
	public PicPanel(String fileName, int width, int height) {
		ir = new ImageResizer();
		this.width = width;
		this.height = height;
		img = ir.getBufferedImage(fileName, width, height);
		//this.paintComponent(g);
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		g.drawImage(img, 0, 0, width, height, null);
	}
}