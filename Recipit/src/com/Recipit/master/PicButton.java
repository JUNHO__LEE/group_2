package com.Recipit.master;

import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;

/*********************************************
 *			 	이미지 버튼 클래스				 *
 *********************************************
 * @param fileLocate 						 *
 * - 파일의 경로를 받아 이미지 버튼을 생성한다.			 *
 *********************************************/
@SuppressWarnings("serial")
public class PicButton extends JButton {
	private Image img;
	private Image newimg;
	private ImageIcon imgIcon;
	private PicButtonActionListener pbal;
	
	public PicButton(String fileName, int width, int height) {
		pbal = new PicButtonActionListener();
		
		imgIcon = new ImageIcon(fileName);
		img = imgIcon.getImage();
		newimg = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
		imgIcon = new ImageIcon(newimg);
		
		this.setIcon(imgIcon);
		this.setBorderPainted(false);		//버튼 테두리 제거
		this.setContentAreaFilled(false);	//버튼 ContentArea 제거
		this.setFocusable(false);
		this.setHorizontalTextPosition(SwingConstants.CENTER);
		this.setVerticalTextPosition(SwingConstants.BOTTOM);
		this.setAlignmentX(CENTER_ALIGNMENT);
		
		this.addActionListener(pbal);
	}
}