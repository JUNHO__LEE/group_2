package com.Recipit.Lee;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.Component;
import java.awt.Rectangle;
import java.awt.Dimension;
import javax.swing.JTabbedPane;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import javax.swing.Icon;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Label;

import javax.swing.JSplitPane;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import com.sun.javafx.scene.layout.region.LayeredBackgroundPositionConverter;
import com.sun.javafx.tk.Toolkit;

import javax.swing.JLayeredPane;
import javax.swing.JTextPane;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SpringLayout;

public class MyPage extends JPanel{

	JPanel backGrid = new JPanel();
	JPanel prof = new JPanel();
	JPanel prof2 = new JPanel();
	
	JPanel mySelfGrid = new JPanel();
	JPanel pm = new JPanel();//  냉장고쪽 바탕
	JPanel pm1 = new JPanel();// 메인바탕
	JPanel pm2 = new JPanel();// 메시지쪽 배경
	JPanel pm3 = new JPanel();// 냉장고 배경
	JPanel pm4 = new JPanel();// 자기소개쪽 바탕
	
	
	JPanel pm5 = new JPanel(){
		@Override
		public Insets getInsets() {
			return new Insets(20,20,20,20);
		}
	};
	// 이미지 들어갈곳
	

	JPanel mySelf = new JPanel();//자기소개 배경
	JPanel mySelfTxt = new JPanel();// 프로필  소개 글
	
	JPanel cookBack = new JPanel();//음식쪽 바탕
	JPanel cook = new JPanel(); // 음식사진 배경
	JPanel cooktxt = new JPanel(); // 요리명 배경
	
	JLabel pi1 = new JLabel();//information 제목
	JLabel pi2 = new JLabel();//information 이름
	JLabel pi3 = new JLabel();//information 나이
	JLabel pi4 = new JLabel();//information 직업
	
	JLabel profile = new JLabel("프로필",JLabel.CENTER);//
	JLabel name = new JLabel("백종원",JLabel.CENTER);//
	JLabel age = new JLabel("45세",JLabel.CENTER);//
	JLabel job = new JLabel("유치원생",JLabel.CENTER);
	JLabel cookname = new JLabel("불가사리 요리");
	
	JButton edit_btn = new JButton("수정");
	JButton back_btn = new JButton(new ImageIcon("images/Lee_백종원.PNG"));
	JButton cook_btn = new JButton(new ImageIcon("images/Lee_요리.PNG"));
	JButton i1_btn = new JButton(new ImageIcon("images/Lee_I1.PNG"));//i1 버튼 (구현x)
	JButton frige_btn = new JButton(new ImageIcon("images/Lee_fridge.PNG"));//My 냉장고로 이동
	
	JPanel profile_pnl = new JPanel();
	JPanel name_pnl = new JPanel();
	JPanel age_pnl = new JPanel();
	JPanel job_pnl = new JPanel();	
	
	ImageIcon images = new ImageIcon("images/Lee_백종원.PNG"); 

	
	private final JLabel  myInfo = new JLabel(" 내 정보"); //라벨
	private JLabel myImg = new JLabel(new ImageIcon("images/Lee_백종원.PNG")); //본인 이미지
	private final JLabel mys = new JLabel("자기소개",JLabel.CENTER); //자기소개	


	public MyPage() {
		setPreferredSize(new Dimension(300, 600));	
		
		setLayout(new BorderLayout());
		add("North",pm1);
		pm1.setLayout(new BorderLayout());
		pm1.setBackground(Color.PINK);
		pm1.add("East", edit_btn);
		pm1.add("West", myInfo);
		backGrid.setBackground(new Color(255, 228, 225));
		backGrid.setLayout(new GridLayout(3, 1));
		add(backGrid);
		
	///////////////////////////////그리드 첫번째 행 , 프로필///////////////////////////

		backGrid.add(prof);
		prof.setLayout(new GridLayout(1, 2));
		prof.setBackground(new Color(72, 61, 139));
		pm5.setBackground(new Color(255, 228, 225));
		
		prof.add(pm5);
		pm5.add(myImg);
		pm5.setBorder(new LineBorder(Color.BLACK));
		
		prof.add(prof2);
		prof2.setLayout(new GridLayout(4, 1));
		prof2.setBorder(new LineBorder(Color.BLACK));
		
		
		profile_pnl.setLayout(new BorderLayout());
		name_pnl.setLayout(new BorderLayout());
		age_pnl.setLayout(new BorderLayout());
		job_pnl.setLayout(new BorderLayout());
		
		profile_pnl.setBorder(new LineBorder(Color.BLACK));
		name_pnl.setBorder(new LineBorder(Color.BLACK));
		age_pnl.setBorder(new LineBorder(Color.BLACK));
		job_pnl.setBorder(new LineBorder(Color.BLACK));
		
		profile_pnl.setBackground(new Color(72, 61, 139));
		name_pnl.setBackground(new Color(169, 169, 169));
		age_pnl.setBackground(new Color(169, 169, 169));
		job_pnl.setBackground(new Color(169, 169, 169));
		profile.setForeground(Color.WHITE);		 
		
		profile_pnl.add(profile);
		profile.setFont(new Font("굴림", Font.BOLD, 20)); 
		name_pnl.add(name);
		age_pnl.add(age);
		job_pnl.add(job);
	
		prof2.add(profile_pnl);
		prof2.add(name_pnl);
		prof2.add(age_pnl);
		prof2.add(job_pnl);
		mySelfGrid.setBackground(new Color(255, 228, 225));
		
	//////////////////////////////////////////////////////////////////////////////
		
		
	////////////////////////////////그리드 2번째 행 냉장고, 자기소개///////////////////////

		backGrid.add(mySelfGrid);
		mySelfGrid.setLayout(new GridLayout(2, 1));
		mySelfGrid.add(pm);
		pm.setLayout(new GridLayout(1, 2));
		pm2.setBackground(new Color(255, 228, 225));
		pm.add(pm2);
		pm2.add(i1_btn);
		pm3.setBackground(new Color(255, 228, 225));
		pm.add(pm3);
		pm3.add(frige_btn);
		
		mySelfGrid.add(pm4);
		pm4.setLayout(new BorderLayout());
		pm4.add(mySelf);
		mySelf.setLayout(new BorderLayout());
		mySelf.setBackground(new Color(221, 160, 221));
		mySelf.add(mys);
		mySelfTxt.setBackground(new Color(255, 228, 225));
		pm4.add("South", mySelfTxt);
		mySelfTxt.setLayout(new BorderLayout());
		mySelfTxt.add("East",new JLabel("프로필 소개"));
		
	/////////////////////////////////////////////////////////////////////////////
		
		
	////////////////////////////그리드 3번째 행 , 음식사진////////////////////////////		

		backGrid.add(cook);
		cook.setLayout(new BorderLayout());
		cook.add(cook_btn);
		cooktxt.setBackground(new Color(255, 228, 225));
		cook.add("South", cooktxt);
		cooktxt.setLayout(new BorderLayout());
		cooktxt.add("East", cookname);

	////////////////////////////////////////////////////////////////////////////
	}
}