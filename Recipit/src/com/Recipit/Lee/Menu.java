package com.Recipit.Lee;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import java.awt.CardLayout;

/* setsize (300, 600);  창 사이즈
 * setreizable(false);  사이즈 고정
 * */

public class Menu extends JFrame {
	
	MyPage myPage = new MyPage();
	BookMark bookMark = new BookMark();
	MyWriting myWriting = new MyWriting();
	Setting setting = new Setting();
	JPanel p1 = new JPanel();
	
	JButton tebMy = new JButton(new ImageIcon("tebMy.PNG"));
	JButton tebok = new JButton(new ImageIcon("tebBook.PNG"));
	JButton tebPen = new JButton(new ImageIcon("tebPen.PNG"));
	JButton tebSet = new JButton(new ImageIcon("tebSet.PNG"));
	JButton b1 = new JButton("닫기");
	public Menu() {
		
		b1.addActionListener((e)->{
			this.dispose();
		});
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.LEFT);
		p1.setBackground(new Color(255, 228, 225));
		p1.setLayout(new CardLayout(0, 0));
		//메뉴바 (마지막에 시간남을때 이미지 추가)
		tabbedPane.setBounds(15, 10, 270, 500);
		tabbedPane.add("마이페이지",myPage);
		tabbedPane.add("북마크",bookMark);
		tabbedPane.add("내 글",myWriting);
		tabbedPane.add("설정",setting);
		p1.add(tabbedPane);
		
		
		getContentPane().add(p1);
		getContentPane().add(b1, BorderLayout.SOUTH);

		//getContentPane().add(tabbedPane);
		//사이즈 설정
		setBounds(195, 200, 300, 600);
		setUndecorated(true);

		setBounds(600, 200, 300, 600);
		setResizable(false);
		setVisible(true);
		
		}

	public static void main(String[] args) {
		new Menu();
	}
}