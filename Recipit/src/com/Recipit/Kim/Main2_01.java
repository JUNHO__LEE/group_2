
package com.Recipit.Kim;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;



public class Main2_01 extends JPanel{
	JPanel p1 = new JPanel();
	JPanel p2 = new JPanel();
	JPanel p3 = new JPanel();
	JPanel p3_1 = new JPanel();
	JPanel p3_2 = new JPanel();
	JPanel p3_3 = new JPanel();
	JPanel p3_4 = new JPanel();
	JPanel card_panel = new JPanel();
			
	//요일버튼
	JButton dBtn1 = new JButton(new ImageIcon("images/day1.png"));
	JButton dBtn2 = new JButton(new ImageIcon("images/day2.png"));
	JButton dBtn3 = new JButton(new ImageIcon("images/day3.png"));
	JButton dBtn4 = new JButton(new ImageIcon("images/day4.png"));
	JButton dBtn5 = new JButton(new ImageIcon("images/day5.png"));
	JButton dBtn6 = new JButton(new ImageIcon("images/day6.png"));
	JButton dBtn7 = new JButton(new ImageIcon("images/day7.png"));
	
	//식단테마 타이틀 라벨
	JLabel lb2 = new JLabel("식단추천");
	JLabel lb3 = new JLabel("가정용(3~4인) 식단",JLabel.CENTER);
	JLabel lb4 = new JLabel("혼밥용(1인) 식단",JLabel.CENTER);
	JLabel lb5 = new JLabel("어린이 식단",JLabel.CENTER);
	JLabel lb6 = new JLabel("다이어트 식단",JLabel.CENTER);	
	
	//식단테마 버튼
	JButton familyBtn = new JButton(new ImageIcon("images/family.jpg"));
	JButton aloneBtn = new JButton(new ImageIcon("images/alone.jpg"));
	JButton kidsBtn = new JButton(new ImageIcon("images/kids.jpg"));
	JButton dietBtn = new JButton(new ImageIcon("images/diet.jpg"));
	
	JPanel p4 = new Main2_02day_1mon();
	JPanel p5 = new Main2_02day_2tue();
	//JPanel p6 = new Main2_02Day_1MON();
	//JPanel p7 = new Main2_02Day_1MON();
	//JPanel p8 = new Main2_02Day_1MON();
	//JPanel p9 = new Main2_02Day_1MON();
	//JPanel p10 = new Main2_02Day_1MON();
	
	JPanel p11 = new Main2_03infoFamily();

	
	CardLayout cardLayout = new CardLayout();
	public Main2_01() {
		dBtn1.setActionCommand("월");
		dBtn2.setActionCommand("화");
		dBtn3.setActionCommand("수");
		dBtn4.setActionCommand("목");
		dBtn5.setActionCommand("금");
		dBtn6.setActionCommand("토");
		dBtn7.setActionCommand("일");
		familyBtn.setActionCommand("가정식단");
		//요일메뉴 버튼 마우스클릭///////////////////////////////////////////////////
		DayMenuHandler_01 dmh = new DayMenuHandler_01();
		dBtn1.addActionListener(dmh);
		dBtn2.addActionListener(dmh);
		dBtn3.addActionListener(dmh);
		dBtn4.addActionListener(dmh);
		dBtn5.addActionListener(dmh);
		dBtn6.addActionListener(dmh);
		dBtn7.addActionListener(dmh);
		
		familyBtn.addActionListener(dmh);
		/////////////////////////////////////////////////////////////////////
		card_panel.setLayout(cardLayout);
		//기본폰트설정
		Font basic = new Font("맑은고딕", Font.PLAIN, 12);
		

		this.setLayout(new BorderLayout(0, 0));
		p1.setLayout(new FlowLayout(FlowLayout.CENTER)); //왼쪽정렬, default = center
		p2.setLayout(new FlowLayout(FlowLayout.CENTER)); //왼쪽정렬, default = center

		//패널사이즈
		p2.setPreferredSize(new Dimension(300, 36));
		
		dBtn1.setPreferredSize(new Dimension(35,30));
		dBtn2.setPreferredSize(new Dimension(35,30));
		dBtn3.setPreferredSize(new Dimension(35,30));
		dBtn4.setPreferredSize(new Dimension(35,30));
		dBtn5.setPreferredSize(new Dimension(35,30));
		dBtn6.setPreferredSize(new Dimension(35,30));
		dBtn7.setPreferredSize(new Dimension(35,30));
		
		
		//요일메뉴
		p2.add("월",dBtn1);p2.add("화",dBtn2); p2.add("수",dBtn3); p2.add("목",dBtn4); 
		p2.add("금",dBtn5); p2.add("토",dBtn6); p2.add("일",dBtn7);
		this.add(p2,BorderLayout.NORTH);
		
		//가정용식단
		p3_1.setLayout(new BorderLayout());
		p3_1.setBackground(Color.WHITE);//패널배경색
		p3_1.setBorder(new LineBorder(new Color(180, 180, 180)));//패널에 테두리넣기
		familyBtn.setBorderPainted(false);//버튼테두리없애기
		familyBtn.setContentAreaFilled(false); //버튼 투명하게
		lb3.setFont(basic);//기본폰트설정
		p3_1.add(familyBtn);
		p3_1.add(lb3,BorderLayout.SOUTH);
		p3.add(p3_1);
		//혼밥용식단
		p3_2.setLayout(new BorderLayout());
		p3_2.setBackground(Color.WHITE);
		p3_2.setBorder(new LineBorder(new Color(180, 180, 180)));
		aloneBtn.setBorderPainted(false);
		aloneBtn.setContentAreaFilled(false);
		lb4.setFont(basic);
		p3_2.add(aloneBtn);
		p3_2.add(lb4,BorderLayout.SOUTH);
		p3.add(p3_2);
		//어린이식단
		p3_3.setLayout(new BorderLayout());
		p3_3.setBackground(Color.WHITE);
		p3_3.setBorder(new LineBorder(new Color(180, 180, 180)));
		kidsBtn.setBorderPainted(false);
		kidsBtn.setContentAreaFilled(false);
		lb5.setFont(basic);
		p3_3.add(kidsBtn);
		p3_3.add(lb5,BorderLayout.SOUTH);
		p3.add(p3_3);
		//다이어트식단
		p3_4.setLayout(new BorderLayout());
		p3_4.setBackground(Color.WHITE);
		p3_4.setBorder(new LineBorder(new Color(180, 180, 180)));
		dietBtn.setBorderPainted(false);
		dietBtn.setContentAreaFilled(false);
		lb6.setFont(basic);
		p3_4.add(dietBtn);
		p3_4.add(lb6,BorderLayout.SOUTH);
		p3.add(p3_4);
		card_panel.add(p3,"main");
		card_panel.add(p4,"월");
		card_panel.add(p5,"화");
		//card_panel.add(p6,"수");
		//card_panel.add(p7,"목");
		//card_panel.add(p8,"금");
		//card_panel.add(p9,"토");
		//card_panel.add(p10,"일");
		card_panel.add(p11,"가정식단");
		
		this.add(card_panel);
		
	}	
	//요일메뉴 클릭한 버튼 비활성화
	class DayMenuHandler_01 implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			String command = e.getActionCommand();
			cardLayout.show(card_panel, command);
			JButton btn = (JButton)e.getSource();
			dBtn1.setEnabled(true);
			dBtn2.setEnabled(true);
			dBtn3.setEnabled(true);
			dBtn4.setEnabled(true);
			dBtn5.setEnabled(true);
			dBtn6.setEnabled(true);
			dBtn7.setEnabled(true);
			btn.setEnabled(false);
			
		}
		
	}	
	/*public static void main(String[] args) {
		JFrame f = new JFrame();
		Main2_01 mf = new Main2_01();
		f.getContentPane().add(mf);
		f.pack();
		f.setBounds(600, 300, 300, 480);
		f.setResizable(false);
		f.setVisible(true);
		f.setDefaultCloseOperation(3);
	}*/
}

