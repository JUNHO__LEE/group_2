package com.Recipit.Kim;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.TextField;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;




//월~일 요일버튼 눌렀을 때 출력되는 페이지

class Main2_02day_1mon extends JPanel{
	JPanel dayP = new JPanel();
	//JPanel bodyP = new JPanel();
	JPanel bodyTopP = new JPanel();
	JPanel bodyLineP = new JPanel();
	JPanel bodyMorningP = new JPanel();
	JPanel bodyLunchP = new JPanel();
	JPanel bodyDinnerP = new JPanel();
	JPanel bodySnackP = new JPanel();
	JPanel mTitleP = new JPanel();
	JPanel lTitleP = new JPanel();
	JPanel dTitleP = new JPanel();
	JPanel sTitleP = new JPanel();
	
	JPanel bodyBtnP = new JPanel();
	
	JPanel rcP1 = new JPanel();
	JPanel rcP2 = new JPanel();
	JPanel rcP3 = new JPanel();
	JPanel rcP4 = new JPanel();
	
	JPanel bottomP = new JPanel();
	
	JLabel calTitle = new JLabel("일일 섭취 칼로리");
	JLabel calL = new JLabel("Kcal");
	JLabel morningL = new JLabel("아침");
	JLabel lunchL = new JLabel("점심");
	JLabel dinnerL = new JLabel("저녁");
	JLabel snackL = new JLabel("간식");
	JLabel bottomL = new JLabel("이전화면으로 돌아가기");
	
	JLabel rNameL1 = new JLabel("레시피 이름");
	JLabel rNameL2 = new JLabel("레시피 이름");
	JLabel rNameL3 = new JLabel("레시피 이름");
	JLabel rNameL4 = new JLabel("레시피 이름");
	
	JLabel blankL = new JLabel();
	
	
	TextField calTf = new TextField(10);
	
	JButton backB = new JButton(new ImageIcon("images/back.png"));
	JButton homeB = new JButton(new ImageIcon("images/home.png"));
	JButton addBtn = new JButton(new ImageIcon("images/add.png"));
	JButton deleteBtn = new JButton(new ImageIcon("images/delete.png"));
	
	//레시피버튼
	JButton rBtn1 = new JButton(new ImageIcon("images/rBtn1.png"));
	JButton rBtn2 = new JButton(new ImageIcon("images/rBtn1.png"));
	JButton rBtn3 = new JButton(new ImageIcon("images/rBtn1.png"));
	JButton rBtn4 = new JButton(new ImageIcon("images/rBtn1.png"));
	
	public Main2_02day_1mon() {	
		//기본폰트설정
		Font basic = new Font("맑은고딕", Font.PLAIN, 12);
		Font small = new Font("맑은고딕", Font.PLAIN, 11);
	
		
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		//setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		dayP.setLayout(new FlowLayout(FlowLayout.CENTER));
		bodyTopP.setLayout(new FlowLayout(FlowLayout.CENTER));
		bodyLineP.setLayout(new FlowLayout(FlowLayout.CENTER));
		bodyMorningP.setLayout(new FlowLayout(FlowLayout.LEFT));
		bodyLunchP.setLayout(new FlowLayout(FlowLayout.LEFT));
		bodyDinnerP.setLayout(new FlowLayout(FlowLayout.LEFT));
		bodySnackP.setLayout(new FlowLayout(FlowLayout.LEFT));
		bodyBtnP.setLayout(new FlowLayout(FlowLayout.RIGHT));
		bottomP.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		
		//패널사이즈
		dayP.setPreferredSize(new Dimension(300, 36));
		
		//morningL.setPreferredSize(new Dimension(50, 40));
		morningL.setHorizontalAlignment(SwingConstants.LEFT);
		lunchL.setHorizontalAlignment(SwingConstants.CENTER);
		dinnerL.setHorizontalAlignment(SwingConstants.CENTER);
		rNameL1.setHorizontalAlignment(SwingConstants.CENTER);
		rNameL2.setHorizontalAlignment(SwingConstants.CENTER);
		rNameL3.setHorizontalAlignment(SwingConstants.CENTER);
		rNameL4.setHorizontalAlignment(SwingConstants.CENTER);
		
		//하단 버튼 사이즈
		backB.setPreferredSize(new Dimension(30,30));
		homeB.setPreferredSize(new Dimension(30,30));
		//추가삭제 버튼사이즈
		addBtn.setPreferredSize(new Dimension(65,25));
		deleteBtn.setPreferredSize(new Dimension(65,25));				
		
		calTitle.setFont(basic);
		bodyTopP.setBackground(Color.WHITE);
		bodyTopP.add(calTitle); bodyTopP.add(calTf); bodyTopP.add(calL);
		this.add(bodyTopP);
		
		rBtn1.setBorderPainted(false);//버튼테두리없애기
		rBtn1.setContentAreaFilled(false); //버튼 투명하게
		rBtn2.setBorderPainted(false);//버튼테두리없애기
		rBtn2.setContentAreaFilled(false); //버튼 투명하게
		rBtn3.setBorderPainted(false);//버튼테두리없애기
		rBtn3.setContentAreaFilled(false); //버튼 투명하게
		rBtn4.setBorderPainted(false);//버튼테두리없애기
		rBtn4.setContentAreaFilled(false); //버튼 투명하게
		addBtn.setBorderPainted(false);//버튼테두리없애기
		addBtn.setContentAreaFilled(false); //버튼 투명하게
		deleteBtn.setBorderPainted(false);//버튼테두리없애기
		deleteBtn.setContentAreaFilled(false); //버튼 투명하게
				
		//아침
		bodyMorningP.setBackground(Color.WHITE);
		mTitleP.setBackground(Color.WHITE);	
		rcP1.setBackground(Color.WHITE);
		this.add(bodyMorningP);
		bodyMorningP.add(mTitleP);
		mTitleP.add(morningL);
		morningL.setFont(basic);
		rNameL1.setFont(small);
		bodyMorningP.add(rcP1);
		rcP1.setLayout(new BorderLayout(0,0));
		rcP1.add(rBtn1);
		rcP1.add(rNameL1, BorderLayout.SOUTH);
		
		//점심
		bodyLunchP.setBackground(Color.WHITE);
		lTitleP.setBackground(Color.WHITE);
		rcP2.setBackground(Color.WHITE);
		this.add(bodyLunchP);
		bodyLunchP.add(lTitleP);
		lTitleP.add(lunchL);
		lunchL.setFont(basic);
		rNameL2.setFont(small);
		bodyLunchP.add(rcP2);
		rcP2.setLayout(new BorderLayout(0,0));
		rcP2.add(rBtn2);
		rcP2.add(rNameL2, BorderLayout.SOUTH);
		
		//저녁
		bodyDinnerP.setBackground(Color.WHITE);
		dTitleP.setBackground(Color.WHITE);
		rcP3.setBackground(Color.WHITE);
		this.add(bodyDinnerP);
		bodyDinnerP.add(dTitleP);
		dTitleP.add(dinnerL);
		dinnerL.setFont(basic);
		rNameL3.setFont(small);
		bodyDinnerP.add(rcP3);
		rcP3.setLayout(new BorderLayout(0,0));
		rcP3.add(rBtn3);
		rcP3.add(rNameL3, BorderLayout.SOUTH);
		
		//저녁
		bodySnackP.setBackground(Color.WHITE);
		sTitleP.setBackground(Color.WHITE);
		rcP4.setBackground(Color.WHITE);
		this.add(bodySnackP);
		bodySnackP.add(sTitleP);
		sTitleP.add(snackL);
		snackL.setFont(basic);
		rNameL4.setFont(small);
		bodySnackP.add(rcP4);
		rcP4.setLayout(new BorderLayout(0,0));
		rcP4.add(rBtn4);
		rcP4.add(rNameL4, BorderLayout.SOUTH);
		
		//식단추가삭제버튼
		bodyBtnP.setBackground(Color.WHITE);
		bodyBtnP.add(deleteBtn); bodyBtnP.add(addBtn); 
		this.add(bodyBtnP);
		
		bottomP.add(backB); bottomP.add(homeB); //bottomP.add(bottomL);
		this.add(bottomP);
		
	}
	
	//테스트 실행용 메인코드
	public static void main(String[] args) {
		JFrame f = new JFrame();
		Main2_02day_1mon mf = new Main2_02day_1mon();
		f.getContentPane().add(mf);
		f.pack();
		f.setBounds(600, 300, 300, 400);
		f.setResizable(false);
		f.setVisible(true);
		f.setDefaultCloseOperation(3);
	}
}


