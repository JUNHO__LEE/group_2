package com.Recipit.Kim;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;



public class Main2_03infoFamily extends JPanel {
	JPanel pDay = new JPanel();
	JPanel pBottom = new JPanel();
	JScrollPane scroll = new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	JPanel pTitle = new JPanel();
	JPanel pImage = new JPanel();
	JPanel pTextarea = new JPanel();
	JPanel pBtn = new JPanel();
	JPanel bodyP = new JPanel();
	
	JLabel lbTitle = new JLabel(new ImageIcon("images/family_title.jpg"));
	JLabel lbImage = new JLabel(new ImageIcon("images/family_or.jpg"));
	//식단설명부분
	String text = "식단설명~~~~~\n어쩌고 저쩌고~~~~~~~~~\n가정에서 일반적으로 요리해먹기 좋은 레시피로\n5대영양소를 골고루 섭취할 수 있도록 \n짜여진 일반 가정용 식단입니다.";
	JTextArea taLog = new JTextArea(text, 0, 0);
	//식단등록&자세히보기 버튼
	JButton addBtn = new JButton(new ImageIcon("images/add2.png")); //나의식단으로 등록
	JButton detailBtn = new JButton(new ImageIcon("images/detail.png")); //식단 자세히보기
	
	//하단 홈&뒤로 버튼
	JButton backB = new JButton(new ImageIcon("images/back.png"));
	JButton homeB = new JButton(new ImageIcon("images/home.png"));
	
	public Main2_03infoFamily() {	
		//기본폰트설정
		Font basic = new Font("맑은고딕", Font.PLAIN, 12);
		Font small = new Font("맑은고딕", Font.PLAIN, 11);
		Font title = new Font("웹고딕", Font.BOLD, 18);
		
		//레이아웃설정
		this.setLayout(new BorderLayout(0,0));
		pBottom.setLayout(new FlowLayout(FlowLayout.LEFT));
		//bodyP.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
	
		scroll.setPreferredSize(new Dimension(290,80));

		addBtn.setPreferredSize(new Dimension(124,45));
		detailBtn.setPreferredSize(new Dimension(124,45));
		//하단 버튼 사이즈
		backB.setPreferredSize(new Dimension(30,30));
		homeB.setPreferredSize(new Dimension(30,30));
	
		
	
		//제목
		pTitle.setBackground(Color.WHITE);
		lbTitle.setFont(title);
		pTitle.add(lbTitle);
		bodyP.add(pTitle);
		//이미지
		pImage.setBackground(Color.WHITE);
		pImage.add(lbImage);
		bodyP.add(pImage);
		//식단설명
		pTextarea.setBackground(Color.WHITE);
		pTextarea.add(scroll);
		taLog.setEditable(false); //false 입력할 수 없는 상태 -> textArea
		taLog.setForeground(Color.DARK_GRAY);
		scroll.setViewportView(taLog);
		bodyP.add(pTextarea);
		//추가&자세히보기 버튼		
		pBtn.setBackground(Color.WHITE);
		detailBtn.setContentAreaFilled(false); //버튼 투명하게
		addBtn.setContentAreaFilled(false); //버튼 투명하게
		pBtn.add(detailBtn); pBtn.add(addBtn);
		bodyP.setBackground(Color.WHITE);
		bodyP.add(pBtn);
		this.add(bodyP, BorderLayout.CENTER);
		//하단 홈버튼
		pBottom.add(backB); pBottom.add(homeB); //bottomP.add(bottomL);
		this.add(pBottom, BorderLayout.SOUTH);
	}
	

			
	
	//테스트 실행용 메인코드
	public static void main(String[] args) {
		JFrame f = new JFrame();
		Main2_03infoFamily mf = new Main2_03infoFamily();
		f.getContentPane().add(mf);
		f.pack();
		f.setBounds(600, 300, 300, 480);
		f.setResizable(false);
		f.setVisible(true);
		f.setDefaultCloseOperation(3);
	}
}
