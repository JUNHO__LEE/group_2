package com.Recipit.Kim;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.color.ColorSpace;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.AbstractBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.LineBorder;




class main2 extends JFrame {
	JPanel p1 = new JPanel();
	JPanel p2 = new JPanel();
	JPanel p3 = new JPanel();
	JPanel p4 = new JPanel();
	JPanel p5 = new JPanel();
	JPanel p6 = new JPanel();
			
	//요일버튼
	JButton dBtn1 = new JButton(new ImageIcon("images/day1.png"));
	JButton dBtn2 = new JButton(new ImageIcon("images/day2.png"));
	JButton dBtn3 = new JButton(new ImageIcon("images/day3.png"));
	JButton dBtn4 = new JButton(new ImageIcon("images/day4.png"));
	JButton dBtn5 = new JButton(new ImageIcon("images/day5.png"));
	JButton dBtn6 = new JButton(new ImageIcon("images/day6.png"));
	JButton dBtn7 = new JButton(new ImageIcon("images/day7.png"));
	
	//식단테마 타이틀 라벨
	JLabel lb2 = new JLabel("식단추천");
	JLabel lb3 = new JLabel("가정용(3~4인) 식단",JLabel.CENTER);
	JLabel lb4 = new JLabel("혼밥용(1인) 식단",JLabel.CENTER);
	JLabel lb5 = new JLabel("어린이 식단",JLabel.CENTER);
	JLabel lb6 = new JLabel("다이어트 식단",JLabel.CENTER);	
	
	//식단테마 버튼
	JButton familyBtn = new JButton(new ImageIcon("images/family.jpg"));
	JButton aloneBtn = new JButton(new ImageIcon("images/alone.jpg"));
	JButton kidsBtn = new JButton(new ImageIcon("images/kids.jpg"));
	JButton dietBtn = new JButton(new ImageIcon("images/diet.jpg"));
		
	public main2() {
		super("식단추천 main2");
		
		//요일메뉴 버튼 마우스클릭///////////////////////////////////////////////////
		DayMenuHandler dmh = new DayMenuHandler();
		dBtn1.addActionListener(dmh);
		dBtn2.addActionListener(dmh);
		dBtn3.addActionListener(dmh);
		dBtn4.addActionListener(dmh);
		dBtn5.addActionListener(dmh);
		dBtn6.addActionListener(dmh);
		dBtn7.addActionListener(dmh);
		/////////////////////////////////////////////////////////////////////
		
		//기본폰트설정
		Font basic = new Font("맑은고딕", Font.PLAIN, 12);

		setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		/*Component com1 = Box.createVerticalStrut(15);
		Component com2 = Box.createVerticalStrut(15);
		Component com3 = Box.createVerticalStrut(15);*/
		
		
		Box box = Box.createVerticalBox();//박스레이아웃 간격
		
		p1.setLayout(new FlowLayout(FlowLayout.CENTER)); //왼쪽정렬, default = center
		p2.setLayout(new FlowLayout(FlowLayout.CENTER)); //왼쪽정렬, default = center
		p3.setLayout(new FlowLayout(FlowLayout.LEFT));
		p4.setLayout(new FlowLayout(FlowLayout.LEFT));
		p5.setLayout(new FlowLayout(FlowLayout.LEFT));
		p6.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		dBtn1.setPreferredSize(new Dimension(35,30));
		dBtn2.setPreferredSize(new Dimension(35,30));
		dBtn3.setPreferredSize(new Dimension(35,30));
		dBtn4.setPreferredSize(new Dimension(35,30));
		dBtn5.setPreferredSize(new Dimension(35,30));
		dBtn6.setPreferredSize(new Dimension(35,30));
		dBtn7.setPreferredSize(new Dimension(35,30));
		
		//식단추천 탭 
		lb2.setFont(basic);
		p1.add(lb2);
		this.add(p1);
		
		//요일메뉴
		p2.add("월",dBtn1);p2.add("화",dBtn2); p2.add("수",dBtn3); p2.add("목",dBtn4); p2.add("금",dBtn5); p2.add("토",dBtn6); p2.add("일",dBtn7);
		this.add(p2);
		
		//가정용식단
		p3.setLayout(new BorderLayout());
		p3.setBackground(Color.WHITE);//패널배경색
		p3.setBorder(new LineBorder(new Color(180, 180, 180)));//패널에 테두리넣기
		familyBtn.setBorderPainted(false);//버튼테두리없애기
		familyBtn.setContentAreaFilled(false); //버튼 투명하게
		lb3.setFont(basic);//기본폰트설정
		p3.add(familyBtn);
		p3.add(lb3,BorderLayout.SOUTH);
		this.add(p3);
		add(box.createVerticalStrut(10));//위아래 간격
		//this.add(com1);
		
		//혼밥용식단
		p4.setLayout(new BorderLayout());
		p4.setBackground(Color.WHITE);
		p4.setBorder(new LineBorder(new Color(180, 180, 180)));
		aloneBtn.setBorderPainted(false);
		aloneBtn.setContentAreaFilled(false);
		lb4.setFont(basic);
		p4.add(aloneBtn);
		p4.add(lb4,BorderLayout.SOUTH);
		this.add(p4);
		add(box.createVerticalStrut(10));//위아래 간격
		//this.add(com2);
		
		//어린이식단
		p5.setLayout(new BorderLayout());
		p5.setBackground(Color.WHITE);
		p5.setBorder(new LineBorder(new Color(180, 180, 180)));
		kidsBtn.setBorderPainted(false);
		kidsBtn.setContentAreaFilled(false);
		lb5.setFont(basic);
		p5.add(kidsBtn);
		p5.add(lb5,BorderLayout.SOUTH);
		this.add(p5);
		add(box.createVerticalStrut(10));//위아래 간격
		//this.add(com3);
		
		//다이어트식단
		p6.setLayout(new BorderLayout());
		p6.setBackground(Color.WHITE);
		p6.setBorder(new LineBorder(new Color(180, 180, 180)));
		dietBtn.setBorderPainted(false);
		dietBtn.setContentAreaFilled(false);
		lb6.setFont(basic);
		p6.add(dietBtn);
		p6.add(lb6,BorderLayout.SOUTH);
		this.add(p6);
		
		//getContentPane().setBackground(Color.DARK_GRAY); //배경색
		pack();
		setBounds(600, 300, 300, 600);
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(3);
		
	}
	
	//요일메뉴 클릭한 버튼 비활성화
	class DayMenuHandler implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			String command = e.getActionCommand();
			//cardLayout.show(card_panel, command);//눌렸을때 보여주는 사진
			JButton btn = (JButton)e.getSource();
			dBtn1.setEnabled(true);
			dBtn2.setEnabled(true);
			dBtn3.setEnabled(true);
			dBtn4.setEnabled(true);
			dBtn5.setEnabled(true);
			dBtn6.setEnabled(true);
			dBtn7.setEnabled(true);
			btn.setEnabled(false);			
		}
		
	}	
	
}
public class Main2_KDH {
	public Main2_KDH() {
		new Main2_KDH();
	}
}
